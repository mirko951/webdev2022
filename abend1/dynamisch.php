<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<?php
    /*
        Das sind die ersten Schritte mit PHP
        WIFI, im Dezember... es regnet!
    */

    // Dies ist ein einzeiliger Komentar
    $i = 'Hallo Welt';
    var_dump($i);

    echo "<hr>";

    $i = 5 + 5; // dies ist eine Addition
    var_dump($i);

    echo "<hr>";

    $j = 5.2356789;
    var_dump($j);

    echo "<hr>";

    $i = $i / 3;
    var_dump($i);

    echo "<hr>";

    $wahrheit = false;
    var_dump($wahrheit);

?>
</body>
</html>
