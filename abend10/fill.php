<?php
// https://www.php.net/manual/de/language.types.boolean.php
// https://www.php.net/manual/de/function.strpos.php
var_dump( (bool)0 );

$str = 'Alle Jahre wieder...';

// Hier ist es ok:
if (false !== strpos($str, 'Alle')) {
    echo "Suche erfolgreich - passt!";
} else {
    echo "kein Treffer";
}

echo '<hr>';

// Hier ist der Bug:
if (strpos($str, 'Alle')) {
    echo "Suche erfolgreich - passt!";
} else {
    echo "kein Treffer";
}

echo '<hr>';

// Bei einer Zuweisung wird zuerst der rechte Teil interpretiert:
// var_dump(strpos($str, 'Alle'));
if (false !== $pos = strpos($str, 'Alle')) {
    echo "Suche erfolgreich - an Position $pos!";
} else {
    echo "kein Treffer";
}

echo '<hr>';

// Hier der Bug:
if ($pos = strpos($str, 'Alle') !== false) {
    echo "Suche erfolgreich - an Position $pos!";
} else {
    echo "kein Treffer";
}

echo '<hr>';

// Diese Lösung ginge auch:
if (($pos = strpos($str, 'Alle')) !== false) {
    echo "Suche erfolgreich - an Position $pos!";
} else {
    echo "kein Treffer";
}

echo '<hr>';

// var_dump( true || false );
print('a') or print('b');

echo '<hr>';

var_dump ( true and false);

$bool = true and false;
var_dump ( $bool );

$bool = true && false;
var_dump ( $bool );
