<?php
require_once 'inc/config.inc.php';
include 'templates/header.php';
$ausgabe = fopen(DB_FILE, 'r');

echo '<table>';

$Anzahl = 0;

// fgetcsv() liefert die Daten direkt für jede Zeile als Array
while ($zeile = fgetcsv($ausgabe)) {
    $Anzahl++;
    echo '<tr>';
    // Klassische For Schleife für ein Array S.254--> for(Startanweisung;Bedingung;Durchlaufanweisung)/count() zählt die Menge der Elemente innerhalb eines Arrays/ 
    $countrypos = count($zeile)-1;
    if ($zeile[$countrypos] == 'AT') {
        $class = 'at';
    } elseif ($zeile[$countrypos] == 'DE') {
        $class = 'de';
    } elseif ($zeile[$countrypos] == 'CH') {
        $class = 'ch';
    } else {
        $class = '';
    }
    for ($i = 0; $i < count($zeile); $i++) {
        // Letzter Index des Arrays wird geprüft, -1 weil bei 0 beginnend
        //Ausgabe der jeweiligen Zeile und Zuweisung zur "class"
        echo "<td class='$class'>$zeile[$i]</td>";
    }
    echo '</tr>';
}

fclose($ausgabe);
echo '</table>';
echo 'Anzahl der Registrierungen: ' . $Anzahl;
include 'templates/footer.php';
