<?php
/*
Logische || (oder) ist nichts anderes als dies:
if (bedingung1 || bedingung2) {
    // anweisungsblock1
}

if (bedingung1) {
    // anweisungsblock1
}
elseif (bedingung2) {
    // anweisungsblock1
}

Teste:
if (!isset($_POST['firstname']) || $_POST['firstname'] == '')
vs.
if ($_POST['firstname'] == '' || !isset($_POST['firstname']))

---

Logische && (und) ist nichts anderes als dies:
if (bedingung1 && bedingung2) {
    // anweisungsblock2
}

if (bedingung1) {
    if (bedingung2) {
        // anweisungsblock
    }
}

Teste:
if (isset($_POST['firstname']) && $_POST['firstname'] != '')
vs.
if ($_POST['firstname'] != '' && isset($_POST['firstname']))
*/
