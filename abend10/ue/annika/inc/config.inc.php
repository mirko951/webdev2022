<?php
require_once 'inc/functions.inc.php';
// Konstante definieren, mit Schlüsselwort "const" - Name der Konstante in Großbuchstaben (PSR)
const DB_FILE = 'data/registrations.csv';
// Alternative Schreibweise: define('DB_FILE', 'registrations.csv'); Unterschied: wird erst zur Laufzeit definiert
// siehe: https://www.php.net/manual/de/language.constants.syntax.php
const APP_NAME = 'DALL-E Contest';
