<?php
require_once 'inc/config.inc.php';
include 'templates/header.php';

$isvalid = validateRegisterForm();
if ($isvalid) {
    // TODO file upload

    // save to CSV:
    saveRegistration();
    echo 'Vielen Dank und viel Glück!';
} else {
    echo 'Bitte füllen Sie alle Felder aus!';
}
include 'templates/footer.php';
