<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <title><?= APP_NAME ?></title>
    <style>
    .table {
        font-size: 1rem;
        border-top: 1px solid black;
        border-right: 1px solid black;
        border-left: 1px solid black;
    }

    .table td {
        border-bottom: 1px solid black;

    }

    .table th,
    td {
        border-bottom: 1px solid black;
        padding: 1rem 2rem;
        text-align: left;
    }

    .tableAT {
        background-color: green !important;
    }

    .tableDE {
        background-color: yellow !important;
    }

    .tableCH {
        background-color: red !important;
    }
    </style>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1><?= APP_NAME ?></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
