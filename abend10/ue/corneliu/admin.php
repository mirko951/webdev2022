<?php

$csvFile = fopen("./data/registrations.csv", "r") or die("Unable to open file!");

$tableArray = array();
while (!feof($csvFile)) {
    $line = fgets($csvFile);
    if (strlen($line) !== 0) {
        $dataArray = explode(",", $line);
        $tableArray[] = $dataArray;
        // array_push($tableArray, $dataArray);
    }
}
fclose($csvFile);

$headers = array("Vorname", "Nachname", "Straße", "PLZ", "Stadt", "Staat");

?>


<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="./css/style.css">
    </head>

    <body>

        <table border ="1">

            <tr>
            <?php foreach ($headers as $header): ?>
                <th><?php echo $header; ?></th>
            <?php endforeach; ?>
            </tr>

        <?php foreach ($tableArray as $data_cell): ?>

            <tr>
            <?php for ($bi = 0; $bi < count($headers); $bi ++): ?>
                <?php if ($bi != 5): ?>
                    <td><?php echo $data_cell[$bi]; ?></td>
                <?php endif; ?>
                <?php if ($bi == 5): ?>
                    <?php echo "<td class= ".$data_cell[$bi].">"?>
                    <?php echo $data_cell[$bi]; ?></td>
                <?php endif; ?>
            <?php endfor; ?>
            </tr>

        <?php endforeach; ?>
        </table>
       
        <?php echo "<div class='table'>Derzeitige Gesamtzahl der Registrieurungen ist:  ".count($tableArray)."</div>"?>

        

    </body>
</html>
