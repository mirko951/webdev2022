<?php
function validateRegisterForm() : bool
{
    // Rückgabewert true/false, weil ich die Ausgabe der Fehlermeldung dann im betreffenden Template machen will!
    // Hier wird nur allgemein geprüft, ob IRGENDETWAS nicht valide ist, aber es wird nicht ersichtlich, WAS nicht valide ist!
    // Warum? Weil natürlich auch die Browser/HTML Validierung aktiviert sein sollte - d.h. die Fehlermeldungen kommen ohnehin,
    // außer jemand manipuliert das HTML - d.h. diese PHP-Validierung dient nicht dem User-Interface, sondern nur der Sicherheit.

    // Freiwillige Übungsmöglichkeit: $errors = [];
    // Wenn eine Überprüfung fehlschlägt, könnte der jeweilige Fehler eingefügt werden: $errors[] = "Fehlermeldung";
    // Statt false, $errors zurückgeben!

    if (empty(trim($_POST['firstname']))) {
        return false;
    }
    if (empty(trim($_POST['lastname']))) {
        return false;
    }
    if (empty(trim($_POST['address']))) {
        return false;
    }
    if (empty(trim($_POST['postal']))) {
        return false;
    }
    if (empty(trim($_POST['city']))) {
        return false;
    }
    if (empty(trim($_POST['country']))) {
        return false;
    }
    if (is_numeric($_POST['postal']) === false) {
        return false;
    }
    $len = strlen($_POST['postal']);
    if ($len < 4 || $len > 5) {
        return false;
    }

    return true;
}

function saveRegistration()
{
    // Dateioperationen:
    // 1. Datei öffnen (https://www.php.net/manual/en/function.fopen):
    $fp = fopen(DB_FILE, 'a');
    // 2. in die Datei schreiben (https://www.php.net/manual/de/function.fputcsv.php):
    fputcsv($fp, $_POST);
    // 3. schließen der datei (https://www.php.net/manual/de/function.fclose.php):
    fclose($fp);
}
