<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registrierung</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    
</head>
<body>
    

<?php

$csv = 'data/registrations.csv';

$handle =fopen($csv, 'r'); 

$head = fgetcsv($handle, 1000, ","); 

$all_record_arr = [];

while ( false !== ($data =fgetcsv ($handle, 1000, ",")) )
{
    $all_record_arr[] =$data; 
}

fclose($handle); 
?>

<h1>Registration Data</h1>

<table border='2' cellspacing='0'>
    <thead>
        <?php foreach ($head as $th) {
            echo "<th>$th</th>";
        } ?>
    </thead>
    <tbody>
        <?php foreach ($all_record_arr as $rec) {
                $class = strtolower($rec[5]);
            ?>
            <tr class="<?= $class ?>">
                <?php foreach ($rec as $cell) {
                    echo '<td>' . $cell . '</td>';
                } ?>
            </tr>
            <?php }
            ?>
    </tbody>
</table>

<?php
        echo count($all_record_arr)  .  'Registrierungen';
?>

</body>
</html>