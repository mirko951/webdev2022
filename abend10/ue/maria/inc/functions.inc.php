<?php
function validateRegisterForm() : bool | string {

    $error = '';
        if (empty($_POST['firstname'])) 
        {
            $error .= '<p><label class="fehler"> Bitte Namen eingeben </label></p>'; 
        }
        else
        {
            $firstname = trim($_POST["firstname"]);
            if (!preg_match("/^[a-zA-Z]*$/",$firstname))
            {
                $error .= '<p><label class="fehler"> Nur Buchstaben und Leerzeichen erlaubt</label></p>';
            }
        }
        if (empty($_POST['lastname'])) 
        {
            $error .= '<p><label class="fehler">Bitte Namen eingeben</label></p>'; 
        }
        else
        {
            $lastname = trim($_POST["lastname"]);
            if(!preg_match("/^[a-zA-Z]*$/",$lastname))
            {
                $error .= '<p><label class="fehler">Nur Buchstaben und Leerzeichen erlaubt</label></p>'; 
            }
        }

        if(empty($_POST['address']))
        {
            $error .= '<p><label class="fehler">Bitte Addresse eingeben</label></p>';
        }
        else
        {
            $address = trim($_POST["address"]); 
            if(!preg_match("/^[a-zA-Z]*$/",$address))
            {
                $error .= '<p><label class="fehler">Nur Buchstaben und Leerzeichen erlaubt</label></p>'; 
            }
        }
        if (empty(trim($_POST['postal'])))
        {
            $error .= '<p>Postal fehlt</p>';
        }
        if (empty(trim($_POST['city']))) {
            $error .= '<p>City fehlt</p>';
        }
        if (empty(trim($_POST['country']))) {
            $error .= '<p>Country fehlt</p>';
        }
        if (is_numeric($_POST['postal']) === false) {
            $error .= '<p>Postal falsch</p>';
        }
        $len = strlen($_POST['postal']);
        if ($len < 4 || $len > 5)
        {
            $error .= '<p>Postal Länge falsch.</p>';
        }
    if ($error !== '') {
        return $error;
    }
    return true;
}
