<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sortierung</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card-header">
                    <h4>Sortierung nach Nachnamen</h4>
                </div>
                <div class="card-body">

                    <form action="" method="GET">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="input-group mb-3">
                                    <select name="sort_alphabet" class="form-control">
                                        <option value="">-- Select Option--</option>
                                        <option value="a-z" 
                                            <?php if(isset($_GET['sort-alphabet']) && $_GET['sort-alphabet'] == "a-z")
                                            { echo "selected";} ?> >A-Z (Ascending Order)</option>
                                        <option value="z-a"
                                             <?php if(isset($_GET['sort-alphabet']) && $_GET['sort-alphabet'] == "z-a")
                                            { echo "selected";} ?> >Z-A (Descending Order)</option>
                                    </select>
                                <button type="submit" class="input-group-text btn btn-primary" id="basic-addon2">
                                    Sort

                                </button>
                                         </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                    <div class="table-respponsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Address</th>
                                    <th>Postal</th>
                                    <th>City</th>
                                    <th>Country</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php

                                    $csv='data/registrations.csv';

                                    $handle =fopen($csv, 'r'); 

                                    $data = fgetcsv($handle, 1000, ","); 

                                    $all_record_arr = [];

                                    $sort_option = "";
                                    if(isset($_GET['sort_alphabet']))
                                    {
                                        if($_GET['sort_alphabet'] == "a-z")
                                        {
                                        $sort_option = "ASC"; 
                                        }
                                        elseif($_GET['sort_alphabet'] == "z-a")
                                        {
                                        $sort_option = "DESC";
                                        }
                                    }

                                    // $query = "SELECT * FROM registrations ORDER BY name $sort_option";
                                    
                                        foreach($all_record_arr as $sort_option)
                                        { 
                                            ?>
                                        
                                        <tr>
                                            <td><?= $row['first_name']; ?></td>
                                            <td><?= $row['last_name']; ?></td>
                                            <td><?= $row['address']; ?></td>
                                            <td><?= $row['postal']; ?></td>
                                            <td><?= $row['city']; ?></td>
                                            <td><?= $row['country']; ?></td>
                                        </tr>

                                     <?php
                                        }
                                        else 
                                        {
                                            ?>
                                            <tr>
                                                <td colspan="6">No Records Found</td>
                                            </tr>
                                        
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

  
</body>
</html>