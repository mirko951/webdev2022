<?php
require_once 'inc/config.inc.php';
include 'templates/header.php';

$csv = readCSV();

echo '<table border="1">';

foreach ($csv as $row) {
    echo '<tr>';
    $class = strtolower($row[5]);
    for ($j = 0; $j < count($row); $j++) {
        echo '<td class="'.$class.'">';
        echo $row[$j];
        echo '</td>';
    }
    echo '</tr>';
}

echo '</table>';
echo ' Gesamtanzahl der Registrierungen: ' . count($csv);
