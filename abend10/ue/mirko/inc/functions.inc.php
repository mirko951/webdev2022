<?php
function validateRegisterForm() : array
{
    $errors = [];

    if (empty(trim($_POST['firstname']))) {
        $errors[] = "Bitte geben Sie den Vornamen ein.";
    }
    if (empty(trim($_POST['lastname']))) {
        $errors[] = "Bitte geben Sie die Nachname ein.";
    }
    if (empty(trim($_POST['address']))) {
        $errors[] = "Bitte geben Sie eine Adresse ein";
    }
    if (empty(trim($_POST['postal']))) {
        $errors[] = "Bitte geben Sie eine Postleitzahl ein";
    }
    if (empty(trim($_POST['city']))) {
        $errors[] = "Bitte geben Sie eine Stadt ein";
    }
    if (empty(trim($_POST['country']))) {
        $errors[] = "Bitte geben Sie ein Land ein";
    }
    if (is_numeric($_POST['postal']) === false) {
        $errors[] = "Das Postleitzahlenfeld darf nur Zahlen enthalten";
    }
    $len = strlen($_POST['postal']);
    if ($len < 4 || $len > 5) {
        $errors[] = "Das Feld für die Postleitzahl darf nur 4 oder 5 Ziffern enthalten.";
    }

    if (count($errors) === 0) {
        echo "Vielen Dank und viel Glück!";
    }

    return $errors;
}


function saveRegistration()
{
    // Dateioperationen:
    // 1. Datei öffnen (https://www.php.net/manual/en/function.fopen):
    $fp = fopen(DB_FILE, 'a');
    // 2. in die Datei schreiben (https://www.php.net/manual/de/function.fputcsv.php):
    fputcsv($fp, $_POST);
    // 3. schließen der datei (https://www.php.net/manual/de/function.fclose.php):
    fclose($fp);
}
function readCSV() : array {
    $filetext = fopen(DB_FILE, 'r');
    $csv = array();
    while (($row = fgetcsv($filetext, null, ",")) !== false) {
        array_push($csv, $row);
    }
    fclose($filetext);
    return $csv;
}
