<?php 
require_once 'inc/config.inc.php';
include 'templates/header.php';


$csv = file(DB_FILE);
sort($csv);
file_put_contents(DB_FILE, $csv);
$data = fopen(DB_FILE, 'r');
echo '<table>';
echo '<thead>';
echo '<tr>';
echo '<th>Vorname</th>';
echo '<th>Nachname</th>';
echo '<th>Adresse</th>';
echo '<th>PLZ</th>';
echo '<th>Ort</th>';
echo '<th>Land</th>';
echo '</tr>';
echo '</thead>';
echo '<tbody>';
$z = 0;
while ($row = fgetcsv($data)) {
    $country = $row[5];
    if ($country === 'DE') {
        echo '<tr style="background-color: yellow;">';
    } elseif ($country === 'AT') {
        echo '<tr style="background-color: green;">';;
    } elseif ($country === 'CH') {
        echo '<tr style="background-color: red;">';;
    }
    foreach ($row as $value) {
        echo '<td>' . $value . '</td>';
    }
    echo '</tr>';
    $z++;
}
// $zeilen = file(DB_FILE);
echo '<tr><td>Anzahl der Einträge:</td><td> ' . $z . '</td></tr>';
fclose($data);

