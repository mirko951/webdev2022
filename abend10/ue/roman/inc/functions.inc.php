<?php


function validateRegisterForm() : array
{
    // Rückgabewert true/false, weil ich die Ausgabe der Fehlermeldung dann im betreffenden Template machen will!
    // Hier wird nur allgemein geprüft, ob IRGENDETWAS nicht valide ist, aber es wird nicht ersichtlich, WAS nicht valide ist!
    // Warum? Weil natürlich auch die Browser/HTML Validierung aktiviert sein sollte - d.h. die Fehlermeldungen kommen ohnehin,
    // außer jemand manipuliert das HTML - d.h. diese PHP-Validierung dient nicht dem User-Interface, sondern nur der Sicherheit.

    // Freiwillige Übungsmöglichkeit: $errors = [];
    // Wenn eine Überprüfung fehlschlägt, könnte der jeweilige Fehler eingefügt werden: $errors[] = "Fehlermeldung";
    // Statt false, $errors zurückgeben!

    $errors = [];
    if (empty(trim($_POST['firstname']))) {
        $errors[] = "Bitte geben Sie einen Vornamen ein!";
    
    }
    if (empty(trim($_POST['lastname']))) {
        $errors[] = "Bitte geben Sie einen Nachnamen ein!";
    
    }
    if (empty(trim($_POST['address']))) {
        $errors[] = "Bitte geben Sie eine Adresse ein!";
    
    }
    if (empty(trim($_POST['postal']))) {
        $errors[] = "Bitte geben Sie eine Postleitzahl ein!";
    
    }
    if (empty(trim($_POST['city']))) {
        $errors[] = "Bitte geben Sie einen Ort ein!";
    
    }
    if (empty(trim($_POST['country']))) {
        $errors[] = "Bitte geben Sie ein Land ein!";
    
    }
    if (is_numeric($_POST['postal']) === false) {
        $errors[] = "Bitte geben Sie eine gültige Postleitzahl ein!";
    
    }
    $len = strlen($_POST['postal']);
    if ($len < 4 || $len > 5) {
        $errors[] = "Bitte geben Sie eine gültige Postleitzahl ein!";
    
    }

    return $errors;
}

function saveRegistration()
{
    // Dateioperationen:
    // 1. Datei öffnen (https://www.php.net/manual/en/function.fopen):
    $fp = fopen(DB_FILE, 'a');
    // 2. in die Datei schreiben (https://www.php.net/manual/de/function.fputcsv.php):
    fputcsv($fp, $_POST);
    // 3. schließen der datei (https://www.php.net/manual/de/function.fclose.php):
    fclose($fp);
}
