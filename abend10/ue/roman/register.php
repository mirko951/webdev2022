<?php
require_once 'inc/config.inc.php';
include 'templates/header.php';

$errors = validateRegisterForm();
if (count($errors) === 0 ) {
    // TODO file upload

    // save to CSV:
    saveRegistration();
    echo 'Vielen Dank und viel Glück!';
} else {
    echo '<ul>';
    foreach ($errors as $error) {
        echo '<li>' . $error . '</li>';
    }
    echo '</ul>';
}
include 'templates/footer.php';

