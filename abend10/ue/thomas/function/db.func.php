<?php

// CSV Datenbank speichern
function saveRegistration()
{
    $db = DB_FILE;

    // Existiert die Datei? Wenn nicht dann Erstellen und Header einfügen
    if(file_exists($db)){
        $fo = fopen($db, 'a');
    }else{
        $fo = fopen($db, 'w');
        $header = ["Vorname", "Nachname", "Adresse", "Postleitzahl", "Stadt", "Land", "Bild"];
        fputcsv($fo,$header); 
    }
    
    $bild_name = 'img/'. $_FILES['bild']['name'];
    if(file_exists($bild_name)){
        $file_name = rand().$_FILES['bild']['name'];
    }else{
        $file_name = $_FILES['bild']['name'];
    }
    $file_tmp = $_FILES['bild']['tmp_name'];
    move_uploaded_file($file_tmp,"img/".$file_name);


    // Formular Daten eintragen
    $inhalt =[
        formular_daten($_POST['vorname']),
        formular_daten($_POST['nachname']),
        formular_daten($_POST['adresse']),
        formular_daten($_POST['plz']),
        formular_daten($_POST['stadt']),
        formular_daten($_POST['land']),
        "img/".$file_name,
    ];

    fputcsv($fo,$inhalt); 
        
    fclose($fo);
}