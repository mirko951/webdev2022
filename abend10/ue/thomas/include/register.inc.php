<?php 
require_once 'function/eingabe.func.php';
session_start();

$vorname = $nachname = $adresse = $plz = $stadt = $bild = "";
$bildtype = ["image/jpeg", "image/jpg", "image/png"];

if(isset($_POST['sendenbtn'])){

    $vorname = formular_daten($_POST['vorname']);
    $nachname = formular_daten($_POST['nachname']);
    $adresse = formular_daten($_POST['adresse']);
    $plz = formular_daten($_POST['plz']);
    $stadt = formular_daten($_POST['stadt']);
    $land = formular_daten($_POST['land']);
    $bild = $_FILES['bild'];

    $bildzielordner = "img/" . $_FILES['bild']['name'];

    $_SESSION['s-vorname'] = $vorname;
    $_SESSION['s-nachname'] = $nachname;
    $_SESSION['s-adresse'] = $adresse;
    $_SESSION['s-plz'] = $plz;
    $_SESSION['s-stadt'] = $stadt;
    $_SESSION['s-land'] = $land;
    $_SESSION['s-bild'] = $bildzielordner;

    if(!preg_match("/^[a-zA-Z ß]*$/" ,$vorname)){
        $errorVorname = 'Bitte beim Vornamen nur Buchstaben eingeben';
    }else{
        if(empty($vorname)){
            $errorVorname = 'Vorname fehlt';
        }
    }

    if(!preg_match("/^[a-zA-Z ß]*$/" ,$nachname)){
        $errorNachname = 'Bitte beim Nachnamen nur Buchstaben eingeben';
    }else{
        if(empty($nachname)){
            $errorNachname = 'Nachnamen fehlt';
        }
    }

    if(!preg_match("/^[a-zA-Z ß]*$/" ,$adresse)){
        $errorAdresse = 'Bitte bei der Adresse nur Buchstaben eingeben';
    }else{
        if(empty($adresse)){
            $errorAdresse = 'Adresse fehlt';
        }    
    }

    if(!preg_match("/^[0-9]*$/" ,$plz)){
        $errorPlz = 'Bitte beim PLZ nur Nummern eingeben';
    }else{
        if(empty($plz)){
            $errorPlz = 'PLZ fehlt';
        }
    }

    if(!preg_match("/^[a-zA-Z ß]*$/" ,$stadt)){
        $errorStadt = 'Bitte bei Stadt nur Buchstaben eingeben';
    }else{
        if(empty($stadt)){
            $errorStadt = 'Stadt fehlt';
        }
    }

    if(!in_array($_FILES['bild']['type'] ,$bildtype)){
        $errorBild = 'Bitte nur .jpg .jpeg .png benutzen';
    }

    if(empty($errorVorname) && empty($errorNachname) && empty($errorAdresse) && empty($errorPlz) && empty($errorStadt) && empty($errorBild)){
        header("Location: register.php");
        saveRegistration();
    }

}else{

    if(empty($vorname)){
        $errorVorname = 'Vorname fehlt';
    }

    if(empty($nachname)){
        $errorNachname = 'Nachnamen fehlt';
    }

    if(empty($adresse)){
        $errorAdresse = 'Adresse fehlt';
    }

    if(empty($plz)){
        $errorPlz = 'PLZ fehlt';
    }

    if(empty($stadt)){
        $errorStadt = 'Stadt fehlt';
    }

    if(empty($bild)){
        $errorBild = 'Bild fehlt';
    }
}