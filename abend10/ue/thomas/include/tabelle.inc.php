<?php

//Spalten aufteilen
$rows   = array_map('str_getcsv', file(DB_FILE));
//Erste Zeile auslesen
$header = array_shift($rows);
//Array mit Schlüsselwort
$csv    = array();
//Zählen der Reihen ohne Header
$count  = count($rows);

// Kombiniert das Array mit Schlüsselwörtern
foreach($rows as $row) {
    $csv[] = array_combine($header, $row);
}

// Sortierung nach Nachname A-Z
foreach($csv as $key => $row2){
    $nachname[$key] = $row2['Nachname'];
}

array_multisort($nachname, SORT_ASC, SORT_STRING, $csv);