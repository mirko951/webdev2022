    <div class="middle">
        <div class="mid oben">
            <div class="alert alert-danger" role="alert">
                <?php include 'include/error.inc.php'?>
            </div>
        </div>
        <div class="mid mitte">
            <form method="POST" enctype="multipart/form-data">
                <div class="input-group mb-3 sm-4">
                    <span class="input-group-text" id="inputGroup-sizing-default"><span class="bi bi-person-circle"> Vorname</span></span>
                    <input type="text" name="vorname" id="vorname" class="form-control" value="<?= $vorname ?>" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
                </div>
                <div class="input-group mb-3">
                    <span class="input-group-text" id="inputGroup-sizing-default"><span class="bi bi-person-circle"> Nachname</span></span>
                    <input type="text" name="nachname" id="nachname" class="form-control" value="<?= $nachname ?>" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
                </div>
                <div class="input-group mb-3">
                    <span class="input-group-text" id="inputGroup-sizing-default"><span class="bi bi-geo-alt-fill"> Adresse</span></span>
                    <input type="text" name="adresse" id="adresse" class="form-control" value="<?= $adresse ?>" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
                </div>
                <div class="input-group mb-3">
                    <span class="input-group-text" id="inputGroup-sizing-default"><span class="bi bi-geo-alt-fill"> Postleitzahl</span></span>
                    <input type="text" name="plz" id="plz" class="form-control" value="<?= $plz ?>" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
                </div>
                <div class="input-group mb-3">
                    <span class="input-group-text" id="inputGroup-sizing-default"><span class="bi bi-geo-alt-fill"> Stadt</span></span>
                    <input type="text" name="stadt" id="stadt" class="form-control" value="<?= $stadt ?>" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
                </div>
                <div class="input-group mb-3">
                    <label class="input-group-text" for="inputGroupSelect01"><span class="bi bi-geo-alt-fill"> Land</span></label>
                    <select name="land" id="land" class="form-select">
                        <option value="AT">Österreich</option>
                        <option value="DE">Deutschland</option>
                        <option value="CH">Schweiz</option>
                    </select>
                </div>
                <div class="input-group mb-5">
                    <label class="input-group-text" for="inputGroupFile01"><i class="bi bi-image"> Bild</i></label>
                    <input type="file" name="bild" id="bild" class="form-control">
                </div>
                <button type="submit" name="sendenbtn" class="btn btn-primary btn-lg"><span class="bi bi-send-fill">Absenden</span></button>
            </form>
        </div>
        <div class="mid unten">

        </div>
    </div>