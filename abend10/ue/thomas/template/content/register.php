    <div class="middle">
        <div class="mid oben">
            <div class="alert alert-success" role="alert">
                <span class="bi bi-check-circle-fill"> Vielen Dank! Ihre Daten wurden übernommen.</span>
            </div>
        </div>
        <div class="mid mitte">
            <div class="table-responsive">
                <table class="table align-middle">
                    <thead>
                        <tr class="table-dark text-center">
                            <th scope="col">Vorname</th>
                            <th scope="col">Nachname</th>
                            <th scope="col">Adresse</th>
                            <th scope="col">Postleitzahl</th>
                            <th scope="col">Stadt</th>
                            <th scope="col">Land</th>
                            <th scope="col">Bild</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="text-center">
                            <td><?php echo $_SESSION['s-vorname']; ?></td>
                            <td><?php echo $_SESSION['s-nachname']; ?></td>
                            <td><?php echo $_SESSION['s-adresse']; ?></td>
                            <td><?php echo $_SESSION['s-plz']; ?></td>
                            <td><?php echo $_SESSION['s-stadt']; ?></td>
                            <td><?php echo $_SESSION['s-land']; ?></td>
                            <td><?php echo '<img src="'. $_SESSION['s-bild'] .'" class="rounded img-fluid" width="150" height="150">' ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="mid unten">
            <form action="index.php" method="POST">
                <button type="submit" name="backbtn" class="btn btn-primary btn-lg"><span class="bi bi-backspace-fill"> Zurück zur Startseite</span></button>
            </form>
        </div>
    </div>