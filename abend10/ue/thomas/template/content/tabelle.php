<div class="middle">
        <div class="mid oben">
            <div class="alert alert-primary" role="alert">
                <span class="bi bi-table"> Adminpanel</span>
            </div>
        </div>
        <div class="mid mitte">
            <div class="table-responsive">
                <table class="table align-middle">
                    <thead>
                        <tr class="table-dark text-center">
                            <?php 
                                // Header Schleife
                                foreach($header as $head){
                                echo '<th>' . $head . '</th>';
                                }
                            ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            // Daten Schleife
                            // Land Tabelle in Verschiedenen Farben
                            for ($i=0;$i<$count;$i++){

                                if($csv[$i]['Land'] == "AT"){
                                    echo '<tr class="table-success text-center">';
                                }elseif(preg_match("[DE]",$csv[$i]['Land'])){
                                    echo '<tr class="table-warning text-center">';
                                }elseif(preg_match("[CH]",$csv[$i]['Land'])){
                                    echo '<tr class="table-danger text-center">';
                                }else{
                                    echo '<tr class="text-center">';
                                }
                        
                                echo '<td>'.$csv[$i]['Vorname'].'</td>';
                                echo '<td>'.$csv[$i]['Nachname'].'</td>';
                                echo '<td>'.$csv[$i]['Adresse'].'</td>';
                                echo '<td>'.$csv[$i]['Postleitzahl'].'</td>';
                                echo '<td>'.$csv[$i]['Stadt'].'</td>';
                                echo '<td>'.$csv[$i]['Land'].'</td>';
                                echo '<td><a href="'.$csv[$i]['Bild'].'" target=_"blank"><img src="'.$csv[$i]['Bild'].'" class="rounded img-fluid" width="150" height="150"></td>';
                                echo '</tr>';      
                            }     
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="mid unten">
            <div class="alert alert-info" role="alert">
                Anzahl der Daten: <?php echo $count ?>
            </div>
            <form action="index.php" method="POST">
                <button type="submit" name="backbtn" class="btn btn-primary btn-lg"><span class="bi bi-backspace-fill"> Zurück zur Startseite</span></button>
            </form>
        </div>
    </div>