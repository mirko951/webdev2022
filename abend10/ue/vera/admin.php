<?php
require_once 'inc/config.inc.php';
include 'templates/header.php';

// Konfiguration
$maxRows = 100;
// $color = '';

// Daten auslesen und Tabelle generieren
$handle = fopen(DB_FILE, "r"); // öffnen der Datei
$counter = 0; // Counter auf 0 starten lassen
echo '<table class="csvTable">';  // HTML Tabelle mit der Klasse csvTable/ ??
while (($data = fgetcsv($handle, 1000, ",")) && ($counter < $maxRows)) {
    if ($data[5] == "AT"){
        echo "<tr bgcolor='green'>";
    }
    elseif ($data[5] == "DE"){
        echo "<tr bgcolor='yellow'>";
    } 
    else {
        echo "<tr bgcolor='red'>";
    }
    
    // echo "<tr>";
    echo "<td>" . $data[0] . "</td>";
    echo "<td>" . $data[1] . "</td>";
    echo "<td>" . $data[2] . "</td>";
    echo "<td>" . $data[3] . "</td>";
    echo "<td>" . $data[4] . "</td>";
    echo "<td>" . $data[5] . "</td>";
 
    $counter++;
}  


// ----------- Anzahl der Zeilen zählen:
// $_content = file("data/registrations.csv");


// -------------- AUSGABE:
echo "</table>";
echo "<p style='text-align:right;'>" . $counter . "</p>";

fclose($handle);

?>






