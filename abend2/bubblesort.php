<?php
$arr = [ 16, 6, 8, 2, 1, 12, 3, 5, 10 ];
// $arr = [ 6, 8, 2, 1, 12, 3, 5, 10, 16 ]; // nach 1. durchlauf
// $arr = [ 6, 2, 1, 8, 3, 5, 10, 12, 16 ]; // nach 2. durchlauf
// $arr = [ 1, 2, 3, 5, 6, 8, 10, 12, 16 ];

/*
setze tauschmarker auf true
setze anzahl der elemente
wiederhole solange tauschmarker true ist
  setze tauschmarker auf false
  setze index-zähler auf 0
  wiederhole solange index-zähler < anzahl der elemente - 1
    wenn das element an index-zähler > element an index-zähler+1
      tausche element an index-zähler mit element an index-zähler+1
      setze tauschmarker auf true
    index-zähler erhöhen
*/

var_dump($arr);
echo "<hr>";

$swapped = true;
$count = count($arr);

while ($swapped == true) {
  $swapped = false;
  // Schönheitsfehler: i=0... anzahl schleifendurchläufe! (ist erledigt in Zeile 39)
  $i = 0;
  while ($i < $count - 1) {
    if ($arr[$i] > $arr[$i+1]) {
      $remember = $arr[$i];
      $arr[$i] = $arr[$i+1];
      $arr[$i+1] = $remember;
      $swapped = true;
    }
    $i++; // das selbe wie: $i = $i + 1;
  }
  $count--; // ist das selbe wie: $count = $count - 1;
}

var_dump($arr);
