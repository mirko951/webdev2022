<?php
// int (integer)
$i = 5;

// float
$j = 5.2;

// bool (boolean)
$k = true;

// null
$l = null;

// string
$m = 'Ich habe $i Finger!\nTest';
echo $m;

$m = "Ich habe $i Finger!\nTest";
echo $m;

echo "<hr>";

// $arr = array(); -> Ist dasselbe wie $arr = [];
$arr = [ 16, 6, 8, 2, 1, 12, 3, 5, 10 ];
// var_dump($arr);
echo $arr[9];
