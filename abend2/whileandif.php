<?php
if (true /* BEDINGUNG - BOOLEAN */) {
    // alles hier in diesem block innerhalb der {} wird ausgeführt, wenn die bedingung true ergibt
    echo "passt!";
}

$i = 10;
if ($i == 10) {
    echo "$i ist 10";
}

echo '<hr>';

$i = 10;
while ($i == 10) {
    echo "$i ist 10";
    $i = $i + 1;
}
