<?php
// indiziertes Array - d.h. in dem Fall werden die Werte/Elemente automatisch indiziert, beginnend bei 0, fortlaufend...
$i_array = [ 5, 'apfel', 'birne', 'marille' ];
$i_array[] = 'zwetschke';
var_dump($i_array);

// assoziativen Array - d.h. die Werte/Elemente bekommen einen eigenen "key" - d.h. eigenen "Namen"/Bezeichner...
$a_array = [
    'firstname' => 'Markus',
    'lastname'  => 'Huber',
    'age'       => 16
];
$a_array['street'] = 'schöne Straße 1';
var_dump($a_array);

echo "<hr>";
echo $i_array[1];

echo "<hr>";
echo $a_array['age'];

echo "<hr>";
foreach ($a_array as $key => $val) {
    echo "$key : $val / ";
}
