<?php
// TODO Multidimensionales Array...
$liste = [
    'firstname' => 'Markus',
    'lastname'  => 'Huber',
    'place'     => [
        [
            'street' => 'Neustraße 5',
            'city'   => 'Vienna'
        ],
        [
            'street' => 'Wiener Straße 4',
            'city'   => 'Vienna'
        ],
        [
            'street' => 'Linzerstraße 50',
            'city'   => 'Vienna'
        ],
    ]
];

$x = $liste['place']; // [ 'street' => 'Neustraße 5', 'city'   => 'Vienna' ]
// echo $x['city'];

echo $liste['place'][1]['street'];
