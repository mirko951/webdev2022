<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        td {
            border: 1px solid black;
        }
    </style>
</head>
<body>
    <table>
        <tr>
        <?php
            $a = [ 2, 5, 7, 8, 9 ];

            // while variante:
            $count = count($a);
            $i = 0;
            while ($i < $count) {
                echo '<td>' . $a[$i] . '</td>'; // -> '<td>9</td>'
                $i++;
            }
            ?>
            </tr>
            <tr>
                <td>x</td>
                <td>x</td>
                <td>x</td>
                <td>x</td>
                <td>x</td>
            </tr>
            <tr>
            <?php
            // for schleife:
            for ($i = 0; $i < $count; $i++) {
                echo '<td>' . $a[$i] . '</td>'; // -> '<td>9</td>'
            }
            ?>
            </tr>
            <tr>
                <td>x</td>
                <td>x</td>
                <td>x</td>
                <td>x</td>
                <td>x</td>
            </tr>
            <tr>
            <?php
            // foreach schleife - nur für arrays (oder alle iterierbare objekte)
            foreach ($a as $val) {
                echo '<td>' . $val . '</td>';
            }
            ?>
            </tr>
            <tr>
                <td>x</td>
                <td>x</td>
                <td>x</td>
                <td>x</td>
                <td>x</td>
            </tr>
            <tr>
            <?php
            // do-while schleife (ist hier fehleranfällig, falls das array leer ist):
            $i = 0;
            do {
                echo '<td>' . $a[$i] . '</td>'; // -> '<td>9</td>'
                $i++;
            } while ($i < $count);
        ?>
        </tr>
    </table>
</body>
</html>
