<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        // echo htmlspecialchars($zahl1);
        if (count($_POST) > 0) {
            $zahl1 = str_replace(',', '', $_POST['zahl1']);
            $zahl2 = str_replace(',', '.', $_POST['zahl2']);

            /*
                Not Operator:
                !true -> false
                !false -> true
            */

            /*
            Wenn wir prüfen wollen, ob mindestens eine der beiden Strings nicht numerisch sind:
            if (!is_numeric($zahl1) || !is_numeric($zahl2)) // das ist das selbe wie hier:
            if (is_numeric($zahl1) == false || is_numeric($zahl2) == false) {
                echo "Bitte nur zahlen eingeben!";
            } else {
            // wenn o.g. ok, dann mit if abfragen, welche operation, und dann eben jeweils:
                echo $zahl1 + $zahl2;
            }
            */

            // Anders herum: prüfen, ob beide Strings numerische Werte enthalten:
            if (is_numeric($zahl1) && is_numeric($zahl2)) {
                /*
                if ($_POST['operator'] == 'add') {
                    echo $zahl1 + $zahl2;
                }
                elseif ($_POST['operator'] == 'sub') {
                    echo $zahl1 - $zahl2;
                }
                elseif ($_POST['operator'] == 'mul') {
                    echo $zahl1 * $zahl2;
                }
                elseif ($_POST['operator'] == 'div') {
                    echo $zahl1 / $zahl2;
                }
                */
                switch ($_POST['operator']) {
                    case 'add':
                        echo $zahl1 + $zahl2;
                        break;
                    case 'sub':
                        echo $zahl1 - $zahl2;
                        break;
                    case 'mul':
                        echo $zahl1 * $zahl2;
                        break;
                    case 'div':
                        echo $zahl1 / $zahl2;
                        break;
                }
            } else {
                echo "Bitte nur zahlen eingeben!";
            }
        }
    ?>
    <form method="POST">
        Zahl1: <input type="text" name="zahl1" required>
        Operator:
            <select name="operator" required>
                <option value="add">+</option>
                <option value="sub">-</option>
                <option value="mul">*</option>
                <option value="div">/</option>
            </select>
        Zahl2: <input type="text" name="zahl2" required><br>
        <input type="submit" value="Senden">
    </form>
</body>
</html>
