<?php
$a = [1, 5, 2, 7, 3, 9, 4, 6, 8];

for ($i = 0; $i < count($a); $i++) {
 
}

foreach ($a as $val) {
    echo $val;
}

function bubbleSort($a) {
    $size = count($a) - 1;
    for ($i = 0; $i < $size; $i++) {
        for ($j = 0; $j < $size - $i; $j++) {
            if ($a[$j] > $a[$j+1]) {
                // Swap elements at indices: $j, $k
                $tmp = $a[$j];
                $a[$j] = $a[$j+1];
                $a[$j+1] = $tmp;
            }
        }
    }
    return $a;
}
