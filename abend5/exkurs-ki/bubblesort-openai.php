<?php
function bubbleSort($arr) {
    for ($i = 0; $i < count($arr); $i++) {
        for ($j = 0; $j < count($arr) - 1; $j++) {
            if ($arr[$j] > $arr[$j + 1]) {
                $temp = $arr[$j];
                $arr[$j] = $arr[$j + 1];
                $arr[$j + 1] = $temp;
            }
        }
    }
    return $arr;
}

$arr = [ 1, 5, 2, 7, 3, 9, 4, 6, 8 ];
$arr = bubbleSort($arr);
for ($i = 0; $i < count($arr); $i++) {
    echo $arr[$i];
    echo "\n";
}
