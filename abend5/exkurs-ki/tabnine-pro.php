<?php
// todo: open csv file
$fp = fopen('text.txt', 'r');

// todo: read from csv file
$csv = fgetcsv($fp);

// read from csv file in a loop
while (($csv = fgetcsv($fp)) !== false) {
    $num = count($csv);
    for ($c = 0; $c < $num; $c++) {
        echo $csv[$c] . ' ';
    }
}

// todo: write to csv file
fputcsv($fp, $csv);

// todo: close csv file
fclose($fp);

// open connection to mysql database with PDO
$pdo = new PDO('mysql:host=localhost;dbname=sakila', 'root', 'root');
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$pdo->query("CREATE DATABASE IF NOT EXISTS sakila");

// read rows from table users
$stmt = $pdo->prepare("SELECT * FROM users");
$stmt->execute();

// insert row into table users
$stmt = $pdo->prepare("INSERT INTO users (name, email) VALUES (:name, :email)");
$stmt->bindValue(':name', 'John Doe', PDO::PARAM_STR);
