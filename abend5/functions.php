<?php
/**
 * Sort an array with bubble sort algorithm
 */
function bubbleSort(array &$arr)
{
    $count = count($arr);
    do {
        $swapped = false;
        for ($i = 0; $i < $count - 1; $i++) {
          if ($arr[$i] > $arr[$i + 1]) {
            $remember = $arr[$i];
            $arr[$i] = $arr[$i + 1];
            $arr[$i + 1] = $remember;
            $swapped = true;
          }
        }
        $count--; // ist das selbe wie: $count = $count - 1;
      } while ($swapped);
    return true;
}

$a1 = [ 6, 4, 8, 1, 0 ];
bubbleSort($a1);
var_dump($a1);

$a2 = [ 6, 0, 2, 1 ];
sort($a2);

// $a2 = bubbleSort($a2);
// var_dump($a2);
