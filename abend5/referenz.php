<?php
// https://www.php.net/manual/de/language.references.php

$x = 5;
// $y bekommt hier eben NICHT den wert der variable $x (das wäre das default-verhalten), sondern eine referenz auf die variable $x
$y = &$x;
// d.h., wenn $x verändert wird, wird auch $y verändert - und umgekehrt
$x = 9;

echo $y;
