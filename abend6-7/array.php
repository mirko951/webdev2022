<?php

$user_assoc = [
    'name'   => 'Max Mustermann',
    'email'  => 'max@mustermann.at',
    'street' => 'Musterstraße 1',
    'zip'    => '12345',
    'city'   => 'Musterstadt',
];

$user = [
    'Max Mustermann',
    'max@mustermann.at',
    'Musterstraße 1',
    '12345',
    'Musterstadt',
];

/*
for ($i = 0; $i < count($user); $i++) {
    // $value = $user[$i];
    $user[$i] = '<strong>' . $user[$i] . '</strong>';
}
*/

// wenn ein wert eines arrays innerhalb einer foreach schleife verändert wird, muss die schleife mit & referenzieren
// da andernfalls nur eine kopie des wertes verändert wird, nicht aber der wert im array selbst
foreach ($user as &$value) {
    $value = '<strong>' . $value . '</strong>';
}

/*
$i = 0;
foreach ($user as $value) {
    $user[$i++] = '<strong>' . $value . '</strong>';
}
*/

foreach ($user_assoc as $key => &$value) {
    $value = '<strong>' . $value . '</strong>';
}

var_dump($user);
var_dump($user_assoc);
