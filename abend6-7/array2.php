<?php
$a = [ 4, 6 ];
$a[] = 12;
$a[] = 13;
$a[] = 14;

var_dump($a);

// wenn ein element aus einem array entfernt wird, wird das array nicht neu indiziert - d.h. es bleibt eine lücke
unset($a[2]);

// array_values() gibt ein neues array zurück, in dem die lücken gefüllt sind
$a = array_values($a);

var_dump($a);

for ($i = 0; $i < count($a); $i++) {
    echo $a[$i];
}
