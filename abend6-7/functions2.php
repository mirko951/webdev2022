<?php
// declare(strict_types=1);

function zinsen(int|float $betrag, float $zinsen, int $jahre = 1) : float
{
    $betrag = $betrag * (1 + $zinsen / 100) ** $jahre;
    return $betrag;
}

echo zinsen('1000', 3);
