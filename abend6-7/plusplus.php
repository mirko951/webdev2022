<?php
$i = 1;

// $x = $i++; // $i += 1; $i = $i + 1;

$x = $i++;

echo "x: $x";

echo "<hr>";

echo "i: $i";

echo "<hr>";

// freiwillige Aufgabe: was ergibt die erste ausgabe, sowie, was ist der wert von $j...
$j = 1;

// $j++: eigentlich 2 werte: $j wird um 1 erhöht; aber der ausdruck ergibt noch den wert vor der erhöhung
// ++$j: hier nur ein wert: $j wird um 1 erhöht; und der ausdruck entspricht schon genau diesen erhöhten wert
echo $j++ + ++$j;
//      1 + 3

echo "<hr>";
echo $j;
//   3
