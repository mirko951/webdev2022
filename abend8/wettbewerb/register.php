<?php
require_once 'inc/config.inc.php';
include 'templates/header.php';

$isvalid = validateRegisterForm();
if ($isvalid) {
    // TODO aufräumen: stil: überprüfung in funktion auslagern!
    // TODO fehlermeldungen ausgeben (alle)!
    // TODO form-submit auf index.php - wenn fehler: dort (mit input vorausgefüllt); wenn ok: redirect!

    // hier spaghetti code:
    $uploadok = true;
    if (isset($_FILES['image'])) {
        $allowed_filetypes = ['jpg', 'jpeg', 'png', 'gif', 'pdf'];
        $filetype = strtolower(pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION));
        if (!in_array($filetype, $allowed_filetypes)) {
            echo "Filetype not allowed!";
            $uploadok = false;
        } else {
            // Limit to 5MB
            if ($_FILES['image']['size'] > 5242880) {
                echo "Sorry, your file is too large.";
                $uploadok = false;
            } else {
                $filename = uniqid() . '.' . $filetype;
                $target = FILE_UPLOADS . $filename;
                echo 'From: ' . $_FILES['image']['tmp_name'];
                echo '<br>';
                echo 'To: ' . $target;
                echo '<br>';
                move_uploaded_file($_FILES['image']['tmp_name'], $target);
            }
        }
    }
    // save to CSV:
    if ($uploadok) {
        saveRegistration($filename);
        echo 'Vielen Dank und viel Glück!';
    }
} else {
    echo 'Bitte füllen Sie alle Felder aus!';
}
include 'templates/footer.php';
