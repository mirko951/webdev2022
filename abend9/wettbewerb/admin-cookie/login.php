<?php
require_once '../inc/config.inc.php';

// Wenn über POST Request, dann Login prüfen und umleiten
if (count($_POST) > 0) {
    // Passwörter sollten verschlüsselt gespeichert werden!
    if ($_POST['user'] === 'admin' && $_POST['password'] === 'admin') {
        setcookie('login', 'ok');
        // User meldet sich an, aber wenn die Userid als Cookie gespeichert wird, dann kann die Benutzerin diese Userid im Browser ja ändern!
        // Also: keine sicherheitsrelevanten Daten als Cookies speichern!!! Cookies werden im Browser gespeichert!
        setcookie('userid', '473264237');
        header('location: index.php');
        exit;
    }
}

include '../templates/header.php';
?>
<form action="login.php" method="post">
    User: <input type="text" name="user"><br>
    Password: <input type="password" name="password"><br>
    <input type="submit" value="Login">
</form>
<?php
include '../templates/footer.php';
