<?php
// https://www.php.net/manual/de/ref.mbstring.php
$str = 'äbc1234';
echo strlen($str);
echo mb_strlen($str);
echo strpos($str, 'b');
echo mb_strpos($str, 'b');

echo '<hr>';

// https://www.php.net/manual/en/language.operators.comparison.php
$str = '111';
if (is_numeric($str)) {
    $output = "alles ok!";
} else {
    $output = "das ist keine zahl!";
}

$output = is_numeric($str) ? "alles ok!" : "das ist keine zahl!";
echo $output;

echo '<hr>';

$x = 'halloa';
echo $x ?? 'nicht gesetzt';
