<?php
require_once '../inc/config.inc.php';
if (!isset($_COOKIE['login']) || $_COOKIE['login'] !== 'ok') {
    // https://www.php.net/manual/de/function.header.php
    header('location: login.php');
    // Wichtig: immer nach einem Umleitungsheader: Programmende! (falls die Umleitung fehlschlägt, oder falls jemand über cURL oder ähnliches den Request ausführt)
    exit;
}
include '../templates/header.php';

/**
 * WICHTIG!
 * Login-Daten sollten NIEMALS in Cookies gespeichert werden!
 * Dies ist nur ein Cookie Beispiel! - Um es danach eben mit Sessions richtig zu machen.
 */

echo "TOP SECRET!!! CSV Daten ausgeben!";

include '../templates/footer.php';
