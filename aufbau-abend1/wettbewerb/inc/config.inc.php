<?php
const PROJECT_ROOT = '/webdev2022/abend9/wettbewerb';
const DB_FILE = 'data/registrations.csv';
const FILE_UPLOADS = 'uploads/';

require_once __DIR__ . '/functions.inc.php';
// Konstante definieren, mit Schlüsselwort "const" - Name der Konstante in Großbuchstaben (PSR)
// Alternative Schreibweise: define('DB_FILE', 'registrations.csv'); Unterschied: wird erst zur Laufzeit definiert
// siehe: https://www.php.net/manual/de/language.constants.syntax.php
const APP_NAME = 'DALL-E Contest';
