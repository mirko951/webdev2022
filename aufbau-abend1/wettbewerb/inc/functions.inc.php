<?php
/**
 * description
 */
function validateRegisterForm() : array
{
    $errors = [];
    if (empty(trim($_POST['firstname']))) {
        $errors[] = "Bitte geben Sie einen Vornamen ein!";
    
    }
    if (empty(trim($_POST['lastname']))) {
        $errors[] = "Bitte geben Sie einen Nachnamen ein!";
    
    }
    if (empty(trim($_POST['address']))) {
        $errors[] = "Bitte geben Sie eine Adresse ein!";
    
    }
    if (empty(trim($_POST['postal']))) {
        $errors[] = "Bitte geben Sie eine Postleitzahl ein!";
    
    } else {
        $len = strlen(trim($_POST['postal']));
        if ($len < 4 || $len > 5 || !is_numeric($_POST['postal'])) {
            $errors[] = "Bitte geben Sie eine 4-5 stellige gültige Postleitzahl ein!";
        }
    }

    if (empty(trim($_POST['city']))) {
        $errors[] = "Bitte geben Sie einen Ort ein!";
    
    }
    if (empty(trim($_POST['country']))) {
        $errors[] = "Bitte geben Sie ein Land ein!";
    }

    if (!empty($_FILES['image']['name'])) {
        $allowed_filetypes = ['jpg', 'jpeg', 'png', 'gif', 'pdf'];
        $filetype = strtolower(pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION));
        if (!in_array($filetype, $allowed_filetypes)) {
            $errors[] = "Bitte laden Sie nur ein Bild hoch (jpg, png, gif, pdf)!";
        } else {
            // Limit to 5MB
            if ($_FILES['image']['size'] > 5242880) {
                $errors[] = "Max. 5MB!";
            }
        }
    }

    return $errors;
}

/**
 * description
 */
function saveRegistration()
{
    if (!empty($_FILES['image'])) {
        $filetype = strtolower(pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION));
        $filename = uniqid() . '.' . $filetype;
        $target = FILE_UPLOADS . $filename;
        move_uploaded_file($_FILES['image']['tmp_name'], $target);
    }

    // Dateioperationen:
    // 1. Datei öffnen (https://www.php.net/manual/en/function.fopen):
    $fp = fopen(DB_FILE, 'a');
    // SQL: INSERT
    // 2. in die Datei schreiben (https://www.php.net/manual/de/function.fputcsv.php):
    // $fieldlist ist zwar nicht notwendig ($_POST ginge auch), aber falls sich die Reihenfolge im Formular ändert, oder neue Eingabefelder hinzukommen, vermeide ich damit einen Fehler in der CSV Fomartierung:
    $fieldlist = [
        $_POST['firstname'],
        $_POST['lastname'],
        $_POST['address'],
        $_POST['postal'],
        $_POST['city'],
        $_POST['country'],
        $target
    ];
    fputcsv($fp, $fieldlist);
    // 3. schließen der datei (https://www.php.net/manual/de/function.fclose.php):
    fclose($fp);
}
