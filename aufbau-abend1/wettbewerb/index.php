<?php
require_once 'inc/config.inc.php';

if (count($_POST) > 0) {
    $errors = validateRegisterForm();
    if (count($errors) === 0) {
        // save file and CSV:
        saveRegistration();
        header("location: thanks.php");
        exit;
    }
}
include 'templates/header.php';

if (isset($errors)) {
    echo '<ul>';
    foreach ($errors as $error) {
        echo '<li>' . $error . '</li>';
    }
    echo '</ul>';
}
?>
                <form action="<?= $_SERVER['PHP_SELF'] ?>" method="post" enctype="multipart/form-data">
                <div class="form-group">
                        <label for="lastname">Lastname</label>
                        <input type="text" name="lastname" id="lastname" value="<?= $_POST['lastname'] ?? ''; ?>" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="firstname">Firstname</label>
                        <input type="text" name="firstname" id="firstname" value="<?= $_POST['firstname'] ?? ''; ?>" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="address">Address</label>
                        <input type="text" name="address" id="address" value="<?= $_POST['address'] ?? ''; ?>" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="postal">Postal</label>
                        <input type="text" name="postal" id="postal" value="<?= $_POST['postal'] ?? ''; ?>" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="city">City</label>
                        <input type="text" name="city" id="city" value="<?= $_POST['city'] ?? ''; ?>" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="country">Country</label>
                        <select name="country" id="country" class="form-control">
                            <option value="AT"<?= isset($_POST['country']) && $_POST['country'] === 'AT' ? ' selected' : '' ?>>Austria</option>
                            <option value="DE"<?= isset($_POST['country']) && $_POST['country'] === 'DE' ? ' selected' : '' ?>>Germany</option>
                            <option value="CH"<?= isset($_POST['country']) && $_POST['country'] === 'CH' ? ' selected' : '' ?>>Switzerland</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="image">Image</label>
                        <input type="file" name="image" id="image" class="form-control" accept='image/*'>
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Submit" class="btn btn-primary">
                    </div>
                </form>
<?php
include 'templates/footer.php';
