<?php
use Dompdf\Dompdf;
/**
 * This class handles the html output rendering...
 */

class PDF implements Output
{
    const HEADER = 'templates/header.php';
    const FOOTER = 'templates/footer.php';

    private Input $inputObject;

    public function __construct(Input $obj)
    {
        $this->inputObject = $obj;
    }

    public function render()
    {
        $pdf = '<table class="table">';
        foreach ($this->inputObject->locations as $location) {
            $pdf .= '<tr>';
            $pdf .= '<td>' . $location->name . '</td>';
            $pdf .= '<td>' . $location->lat . '</td>';
            $pdf .= '<td>' . $location->lon . '</td>';
            $pdf .= '</tr>';
        }
        $pdf .= '</table>';

        $dompdf = new Dompdf();
        $dompdf->loadHtml($pdf);

        $dompdf->setPaper('A4');
        $dompdf->render();
        $dompdf->stream();
    }
}
