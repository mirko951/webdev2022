<?php
/**
 * This class handles the html output rendering...
 */

class HTML implements Output
{
    const HEADER = 'templates/header.php';
    const FOOTER = 'templates/footer.php';

    private Input $inputObject;

    public function __construct(Input $obj)
    {
        $this->inputObject = $obj;
    }

    public function render()
    {
        include self::HEADER;

        echo '<table class="table">';
        foreach ($this->inputObject->locations as $location) {
            echo '<tr>';
            echo '<td>' . $location->name . '</td>';
            echo '<td>' . $location->lat . '</td>';
            echo '<td>' . $location->lon . '</td>';
            echo '</tr>';
        }
        echo '</table>';
        
        include self::FOOTER;
    }
}
