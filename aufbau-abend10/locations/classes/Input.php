<?php
interface Input
{
    public function read(?string $filter = null);
}
