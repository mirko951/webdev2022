<?php
interface Output
{
    public function render();
}
