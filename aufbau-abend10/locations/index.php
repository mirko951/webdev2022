<?php
require_once 'inc/config.php';

$source = $_GET['source'] ?? 'driving';
switch ($source) {
    case 'driving':
        $data = new GeoJSON(API_DRIVING);
        break;
    case 'medical':
        $data = new GeoJSON(API_MEDICAL);
        break;
    case 'church':
        $data = new CSV(API_CHURCH);
        break;
    default:
        $data = new GeoJSON(API_DRIVING);
        break;
}

// $data = new Sightseeing;
$data->read();

/**
 * 5 Ausgabearten:
 * als HTML Tabelle
 * als PDF Dokument
 * als Excel Format
 * als Kartendarstellung
 * als CSV
 * als JSON
 */

$output = new PDF($data);
$output->render();
