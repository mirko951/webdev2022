<?php
/**
 * Desc
 */
class JSON extends Output
{
    public function render()
    {
        header('Content-Type: application/json');
        echo json_encode( [ 'Locations' => $this->inputObject->locations ] );
    }
}
