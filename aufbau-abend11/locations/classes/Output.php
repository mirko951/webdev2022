<?php
/**
 * Desc.
 */
abstract class Output
{
    protected Input $inputObject;

    public function __construct(Input $obj)
    {
        $this->inputObject = $obj;
    }

    protected function htmlTable() : string
    {
        $html = '<table class="table">';
        foreach ($this->inputObject->locations as $location) {
            $html .= '<tr>';
            $html .= '<td>' . $location->name . '</td>';
            $html .= '<td>' . $location->lat . '</td>';
            $html .= '<td>' . $location->lon . '</td>';
            $html .= '</tr>';
        }
        $html .= '</table>';
        return $html;
    }

    abstract public function render();
}
