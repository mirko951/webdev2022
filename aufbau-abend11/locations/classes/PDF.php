<?php
use Dompdf\Dompdf;
/**
 * This class handles the html output rendering...
 */

class PDF extends Output
{
    public function render()
    {
        $pdf = $this->htmlTable();

        $dompdf = new Dompdf();
        $dompdf->loadHtml($pdf);

        $dompdf->setPaper('A4');
        $dompdf->render();
        $dompdf->stream();
    }
}
