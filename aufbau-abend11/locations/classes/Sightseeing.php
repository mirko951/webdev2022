<?php
class Sightseeing implements Input
{
    public array $locations;

    public function __construct()
    {
        $this->locations = [];
    }

    public function read(?string $bla = null)
    {
        // aus datenbank lesen
        $this->locations[] = new Location('test', 16, 48);
        $this->locations[] = new Location('test2', 16, 48);
        $this->locations[] = new Location('test3', 16, 48);
        $this->locations[] = new Location('test4', 16, 48);
    }
}
