var annotated_dup =
[
    [ "CSV", "class_c_s_v.html", "class_c_s_v" ],
    [ "Excel", "class_excel.html", "class_excel" ],
    [ "GeoJSON", "class_geo_j_s_o_n.html", "class_geo_j_s_o_n" ],
    [ "HTML", "class_h_t_m_l.html", "class_h_t_m_l" ],
    [ "Input", "interface_input.html", "interface_input" ],
    [ "JSON", "class_j_s_o_n.html", "class_j_s_o_n" ],
    [ "Location", "class_location.html", "class_location" ],
    [ "Output", "class_output.html", "class_output" ],
    [ "PDF", "class_p_d_f.html", "class_p_d_f" ],
    [ "Sightseeing", "class_sightseeing.html", "class_sightseeing" ],
    [ "Webservice", "class_webservice.html", "class_webservice" ]
];