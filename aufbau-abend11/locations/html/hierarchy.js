var hierarchy =
[
    [ "Input", "interface_input.html", [
      [ "Sightseeing", "class_sightseeing.html", null ],
      [ "Webservice", "class_webservice.html", [
        [ "CSV", "class_c_s_v.html", null ],
        [ "GeoJSON", "class_geo_j_s_o_n.html", null ]
      ] ]
    ] ],
    [ "Location", "class_location.html", null ],
    [ "Output", "class_output.html", [
      [ "Excel", "class_excel.html", null ],
      [ "HTML", "class_h_t_m_l.html", null ],
      [ "JSON", "class_j_s_o_n.html", null ],
      [ "PDF", "class_p_d_f.html", null ]
    ] ]
];