<?php
require_once 'inc/config.php';

$source = $_GET['source'] ?? 'driving';
switch ($source) {
    case 'medical':
        $data = new GeoJSON(API_MEDICAL);
        break;
    case 'church':
        $data = new CSV(API_CHURCH);
        break;
    default:
        $data = new GeoJSON(API_DRIVING);
        break;
}
$data->read();

/**
 * Weitere Möglichkeiten:
 * todo: als Kartendarstellung
 * TODO: als JSON
 */

$dst = $_GET['dst'] ?? 'html';
switch ($dst) {
    case 'pdf':
        $output = new PDF($data);
        break;
    case 'xls':
        $output = new Excel($data);
        break;
    case 'json':
        $output = new JSON($data);
        break;
    default:
        $output = new HTML($data);
        break;
}

$output->render();
