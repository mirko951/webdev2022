<?php
namespace Locations\Input;

class Sightseeing implements Input
{
    public array $locations;

    public function __construct()
    {
        $this->locations = [];
        $db = new \PDO('mysql:asdf');
    }

    public function read(?string $bla = null)
    {
        // aus datenbank lesen
        $this->locations[] = new \Locations\Location('te<b>test</b>st', 16, 48);
        $this->locations[] = new \Locations\Location('test2', 16, 48);
        $this->locations[] = new \Locations\Location('test3', 16, 48);
        $this->locations[] = new \Locations\Location('test4', 16, 48);
    }
}
