<?php
// https://www.php.net/manual/en/language.namespaces.rationale.php
namespace Locations;

/**
 * The class represents a single location
 */
class Location
{
    /** @var string $name contains the name of a single location */
    public ?string $name;

    /** @var float $lat contains the latitude of a location */
    public ?float $lat;

    /** @var float $lon contains the longitude of a location */
    public ?float $lon;

    /**
     * The constructor - sets the name, lat, lon if given; otherwise creates an empty location with null values
     */
    public function __construct(?string $name = null, ?float $lat = null, ?float $lon = null)
    {
        $this->name = $name;
        $this->lat = $lat;
        $this->lon = $lon;
    }
}
