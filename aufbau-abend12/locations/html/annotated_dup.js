var annotated_dup =
[
    [ "Locations", "namespace_locations.html", [
      [ "Input", "namespace_locations_1_1_input.html", [
        [ "CSV", "class_locations_1_1_input_1_1_c_s_v.html", "class_locations_1_1_input_1_1_c_s_v" ],
        [ "GeoJSON", "class_locations_1_1_input_1_1_geo_j_s_o_n.html", "class_locations_1_1_input_1_1_geo_j_s_o_n" ],
        [ "Input", "interface_locations_1_1_input_1_1_input.html", "interface_locations_1_1_input_1_1_input" ],
        [ "Sightseeing", "class_locations_1_1_input_1_1_sightseeing.html", "class_locations_1_1_input_1_1_sightseeing" ],
        [ "Webservice", "class_locations_1_1_input_1_1_webservice.html", "class_locations_1_1_input_1_1_webservice" ]
      ] ],
      [ "Output", "namespace_locations_1_1_output.html", [
        [ "HTML", "class_locations_1_1_output_1_1_h_t_m_l.html", "class_locations_1_1_output_1_1_h_t_m_l" ],
        [ "JSON", "class_locations_1_1_output_1_1_j_s_o_n.html", "class_locations_1_1_output_1_1_j_s_o_n" ],
        [ "Output", "class_locations_1_1_output_1_1_output.html", "class_locations_1_1_output_1_1_output" ],
        [ "PDF", "class_locations_1_1_output_1_1_p_d_f.html", "class_locations_1_1_output_1_1_p_d_f" ]
      ] ],
      [ "Location", "class_locations_1_1_location.html", "class_locations_1_1_location" ]
    ] ],
    [ "Excel", "class_excel.html", "class_excel" ]
];