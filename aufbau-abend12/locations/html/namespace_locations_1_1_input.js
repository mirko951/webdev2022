var namespace_locations_1_1_input =
[
    [ "CSV", "class_locations_1_1_input_1_1_c_s_v.html", "class_locations_1_1_input_1_1_c_s_v" ],
    [ "GeoJSON", "class_locations_1_1_input_1_1_geo_j_s_o_n.html", "class_locations_1_1_input_1_1_geo_j_s_o_n" ],
    [ "Input", "interface_locations_1_1_input_1_1_input.html", "interface_locations_1_1_input_1_1_input" ],
    [ "Sightseeing", "class_locations_1_1_input_1_1_sightseeing.html", "class_locations_1_1_input_1_1_sightseeing" ],
    [ "Webservice", "class_locations_1_1_input_1_1_webservice.html", "class_locations_1_1_input_1_1_webservice" ]
];