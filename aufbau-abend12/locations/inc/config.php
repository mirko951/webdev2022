<?php
require_once 'inc/functions.php';
// Möglichkeit 1: Autoload selbst implementieren: hier in der Funktion "myLoader"
// spl_autoload_register('myLoader');

// Möglichkeit 2: bei Verwendung von Composer, in der composer.json konfigurieren:
// https://getcomposer.org/doc/04-schema.md#psr-4
require 'vendor/autoload.php';

const APP_NAME = 'Locations';

/**
 * The APIs
 */
const API_DRIVING = 'https://data.wien.gv.at/daten/geo?service=WFS&request=GetFeature&version=1.1.0&typeName=ogdwien:FAHRSCHULEOGD&srsName=EPSG:4326&outputFormat=json';
const API_MEDICAL = 'https://data.wien.gv.at/daten/geo?service=WFS&request=GetFeature&version=1.1.0&typeName=ogdwien:ARZTOGD&srsName=EPSG:4326&outputFormat=json';
const API_CHURCH = 'https://data.wien.gv.at/daten/geo?service=WFS&request=GetFeature&version=1.1.0&typeName=ogdwien:RELIGIONOGD&srsName=EPSG:4326&outputFormat=csv';
