<?php
/** AUF XML ummodelln
 * The class handles all XML (Features) input data
 */
class KML extends Webservice {
    public function read(?string $filter = null) {
        $xml = simplexml_load_file($this->url);
        if ($xml === false) {
            echo "Failed to load XML file\n";
            foreach(libxml_get_errors() as $error) {
                echo "\t", $error->message;
            }
            return;
        }
    
        $this->locations = []; // initialize the locations array
    
        echo "Found ", count($xml->Document->Folder), " folders and ", count($xml->Document->Placemark), " placemarks\n";
     
        foreach ($xml->Document->Folder as $folder) {
            echo "Processing folder: ", (string)$folder->name, "\n";
            foreach ($folder->Placemark as $placemark) {
                if (!isset($filter) || strpos((string)$placemark->name, $filter) !== false) {
                    $coordinates = (string)$placemark->Point->coordinates;
                    $points = explode(',', $coordinates);
                    if (count($points) < 2) {
                        echo "Invalid number of coordinates for element: ", (string)$placemark->name, "\n";
                        continue;
                    }
                    $lon = trim($points[0]);
                    $lat = trim($points[1]);
    
                    // Debug statement: check if lat and lon are numeric
                    if (!is_numeric($lat) || !is_numeric($lon)) {
                        echo "Invalid lat or lon for element: ", (string)$placemark->name, "\n";
                        continue;
                    }
    
                    $name = (string)$placemark->name;
                    $this->locations[] = new Location($name, (float)$lat, (float)$lon);
                }
            }
        }
    
        echo "Found ", count($this->locations), " valid locations\n";
    }
}    