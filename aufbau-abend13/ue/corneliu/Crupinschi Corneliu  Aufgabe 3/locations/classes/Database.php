<?php
class Database
{
    protected string $url;
  
    public array $locations;

    private function addLocation($line){
        $values = explode(",",trim($line, "('),"));
        $values[1] = trim($values[1]);
        $values[1] = str_replace("'", '', $values[1]);
        $this->locations[] = new Location($values[1], (float) $values[2], (float) $values[3]);
    }

    public function __construct(string $url)
    {
        $this->url = $url;
        $this->locations = [];
    }

    public function read()
    {
        $file_string = file_get_contents($this->url);

        $startpos = strpos($file_string, 'INSERT INTO');
        $endpos = strpos($file_string, ');', $startpos);
        $substring = substr($file_string, $startpos, $endpos-$startpos);
        $lines = explode("\n", $substring);
        unset($lines[0]);

        foreach ($lines as $line) {
            $this->addLocation($line);
        }
    }
}
