<?php
/**
 * The class handles all KML input data
 */
class KML extends Webservice 
{
    public function read(?string $filter = null)
    {
        $file = simplexml_load_file($this->url);
        foreach ($file->Document->Folder->Placemark as $placemark) {
            $name = (string) $placemark->name;
            $longitude = (float) $placemark->LookAt->longitude;
            $latitude = (float) $placemark->LookAt->latitude;

            $this->locations[] = new Location($name, $longitude, $latitude);
        }
    }
}