<?php
require_once 'inc/functions.php';
spl_autoload_register('myLoader');

const APP_NAME = 'Locations';

/**
 * The APIs
 */

const API_BOOK = 'https://data.wien.gv.at/daten/geo?version=1.3.0&service=WMS&request=GetMap&crs=EPSG:4326&bbox=48.10,16.16,48.34,16.59&width=1&height=1&layers=ogdwien:BUECHEREIOGD&styles=BUECHEREIOGDPNG&format=application/vnd.google-earth.kml+xml';
const API_DATABASE = './sightseeing.sql';
const API_DRIVING = 'https://data.wien.gv.at/daten/geo?service=WFS&request=GetFeature&version=1.1.0&typeName=ogdwien:FAHRSCHULEOGD&srsName=EPSG:4326&outputFormat=json';
const API_MEDICAL = 'https://data.wien.gv.at/daten/geo?service=WFS&request=GetFeature&version=1.1.0&typeName=ogdwien:ARZTOGD&srsName=EPSG:4326&outputFormat=json';
const API_CHURCH = 'https://data.wien.gv.at/daten/geo?service=WFS&request=GetFeature&version=1.1.0&typeName=ogdwien:RELIGIONOGD&srsName=EPSG:4326&outputFormat=csv';
