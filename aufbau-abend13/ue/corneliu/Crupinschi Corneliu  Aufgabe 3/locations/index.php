<?php
require_once 'inc/config.php';

$source = $_GET['source'] ?? 'database';
switch ($source) {
    case 'driving':
        $data = new GeoJSON(API_DRIVING);
        break;
    case 'medical':
        $data = new GeoJSON(API_MEDICAL);
        break;
    case 'church':
        $data = new CSV(API_CHURCH);
        break;
    case 'book':
        $data = new KML(API_BOOK);
        break;
    
    case 'database':
        $data = new Database(API_DATABASE);
        break;
           
    
    default:
        $data = new KML(API_BOOK);
        break;
}
$data->read();

include 'templates/header.php';

echo '<table class="table">';
foreach ($data->locations as $location) {
    echo '<tr>';
    echo '<td>' . $location->name . '</td>';
    echo '<td>' . $location->lat . '</td>';
    echo '<td>' . $location->lon . '</td>';
    echo '</tr>';
}
echo '</table>';

include 'templates/footer.php';
