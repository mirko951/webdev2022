<?php
class Sightseeing {

    private int $id;
    private string $ort;
    private float $lat, $lon;

    public function __construct(array $data)
    {
        $this->id = $data['id'];
        $this->ort = $data['ort'];
        $this->lat= $data['lat'];
        $this->lon = $data['lon'];
    }

    public function getLocationId()
    {
        return $this->id;
    }

    public function getLocationOrt()
    {
        return $this->ort;
    }

    public function getLocationLat()
    {
        return $this->lat;
    }

    public function getLocationLon()
    {
        return $this->lon;
    }
}