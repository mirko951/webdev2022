<?php
class Database {

public $con;

public function __construct(string $host, string $username, string $password, string $database)
    {

    $this->con = mysqli_connect($host, $username, $password, $database);
    }
    
public function getLocation(string $sql)
    {
        $statement = $this->con->prepare($sql);
        $statement->execute();
        $result = $statement->get_result();
        while($row = $result->fetch_assoc())
        {
            $dataSet[] = new Sightseeing ($row);
        }
        if(!empty($dataSet))
            return $dataSet;
        else 
            return null;
    }

}
