<?php
require ("classes/Sightseeing.php");
require("classes/connection.php");

?> 

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sightseeing</title>
</head>
<body>
    
<?php

$db = new Database("localhost","root","","sightseeing");

$dataSet = $db->getLocation ("SELECT * FROM places");

if($dataSet)
{
    foreach($dataSet as $data)
    {
        echo "<p>";

        echo "ID: " . $data->getLocationId() . "<br />";
        echo "Ort: " . $data->getLocationOrt() . "<br />";
        echo "Lat: " . $data->getLocationLat() . "<br />";
        echo "Lon: " . $data->getLocationLon() . "<br />";

        echo "</p>";
    }

}
else 
    echo "No Results";
?>
</body>
</html>