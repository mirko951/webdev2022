<?php
require_once 'inc/config.php';
class Standortdaten {
  private PDO $conn;

  public function __construct() {
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "sightseeing";
    
    // Creating the PDO connection
    try {
      $this->conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
      $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch (PDOException $e) {
      throw new Exception("Impossible to connect to the database: " . $e->getMessage());
    }
  }

  public function read() {
    try {
      // Preparing and executing the query
      $sql = "SELECT * FROM places";
      $stmt = $this->conn->prepare($sql);
      $stmt->execute();

      // Reading the results and printing the data
      // $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
      $results = false;
      echo '<table class="table">';
      while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $results = true;
        echo '<tr>';
        echo '<td>' . $row['ort'] . '</td>';
        echo '<td>' . $row['lat'] . '</td>';
        echo '<td>' . $row['lon'] . '</td>';
        echo '</tr>';
      }
      echo '</table>';
      if (!$results) {
        echo "No results found.";
      }
    } catch (PDOException $e) {
      throw new Exception("Error reading data:" . $e->getMessage() );
    }
  }
}

include 'templates/header.php';

$standortdaten = new Standortdaten();

$standortdaten->read();

include 'templates/footer.php';