<?php

// PHP-Aufbau Pflichtübung Möglichkeit 2


/**
 * The class handles all data of the database table "places"
 */
class Database
{
    public array $locations = [];
    private $db;

    /**
     * This Contructor creates a new object and connects to the Sightseeing database
     */
    public function __construct(array $database)
    {
        //TODO: Datenbank verbindung durch Übergabeparameter -> Mehr als eine DB Verbindung möglich. 
        $this->db = new PDO('mysql:host='.$database['host'].';dbname='.$database['database'], $database['user'], $database['pwd']);
        $this->locations = [];
    }

    public function read(?string $filter = null)
{
    $sql = "SELECT ort, lat, lon FROM places";
    $stmt = $this->db->prepare($sql);
    $stmt->execute();
    while ($location = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $this->locations[] = new Location($location['ort'], $location['lat'], $location['lon']);
    }

}

}














































































