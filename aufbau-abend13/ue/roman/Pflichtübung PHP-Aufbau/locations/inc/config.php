<?php
require_once 'inc/functions.php';
spl_autoload_register('myLoader');

const APP_NAME = 'Locations';

/**
 * The APIs
 */
const API_DRIVING = 'https://data.wien.gv.at/daten/geo?service=WFS&request=GetFeature&version=1.1.0&typeName=ogdwien:FAHRSCHULEOGD&srsName=EPSG:4326&outputFormat=json';
const API_MEDICAL = 'https://data.wien.gv.at/daten/geo?service=WFS&request=GetFeature&version=1.1.0&typeName=ogdwien:ARZTOGD&srsName=EPSG:4326&outputFormat=json';
const API_CHURCH = 'https://data.wien.gv.at/daten/geo?service=WFS&request=GetFeature&version=1.1.0&typeName=ogdwien:RELIGIONOGD&srsName=EPSG:4326&outputFormat=csv';

const DB_SIGHTSEEING = [
    'host' => 'localhost',
    'database' => 'sightseeing',
    'user' => 'root',
    'pwd' => ''
];
