-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 20, 2021 at 11:47 AM
-- Server version: 10.3.25-MariaDB-0ubuntu0.20.04.1
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sightseeing`
--
CREATE DATABASE IF NOT EXISTS `sightseeing` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `sightseeing`;

-- --------------------------------------------------------

--
-- Table structure for table `places`
--

CREATE TABLE `places` (
  `id` int(10) UNSIGNED NOT NULL,
  `ort` varchar(255) NOT NULL,
  `lat` float NOT NULL,
  `lon` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `places`
--

INSERT INTO `places` (`id`, `ort`, `lat`, `lon`) VALUES
(1, 'Ankeruhr', 48.2108, 16.3737),
(2, 'Friedhof St. Marx', 48.183, 16.4017),
(3, 'Kuffner-Sternwarte', 48.2129, 16.2913),
(4, 'Künstlerhaus', 48.201, 16.3713),
(5, 'Parlament', 48.2081, 16.3585),
(6, 'Planetarium', 48.2161, 16.395),
(7, 'Rathaus', 48.2108, 16.3573),
(8, 'Spanische Hofreitschule', 48.2073, 16.3668),
(9, 'Synagoge', 48.2116, 16.3748),
(10, 'Urania Sternwarte', 48.2116, 16.3839),
(11, 'Wiener Staatsoper', 48.2033, 16.3691),
(12, 'Zentralfriedhof (Ehrengräber)', 48.1513, 16.4387),
(13, 'Albertina', 48.2046, 16.3682),
(14, 'Haus der Musik', 48.2038, 16.3731),
(15, 'Haus des Meeres', 48.1976, 16.3529),
(16, 'Jüdisches Museum der Stadt Wien', 48.2071, 16.3694),
(17, 'Kaisergruft (Kapuzinergruft)', 48.2056, 16.3699),
(18, 'KunstHausWien - Friedensreich Hundertwasser', 48.2111, 16.3935),
(19, 'Palais Liechtenstein', 48.2226, 16.3596),
(20, 'MAK - Österreichisches Museum für angewandte Kunst', 48.2076, 16.3816),
(21, 'Mozarthaus Vienna \"Figarohaus\"', 48.2082, 16.3749),
(22, 'MuseumsQuartier Wien', 48.2033, 16.3585),
(23, 'Oberes Belvedere', 48.1916, 16.3809),
(24, 'Österreichische Nationalbibliothek', 48.2057, 16.3654),
(25, 'Augustinerkirche', 48.2055, 16.3678),
(26, 'Burgtheater', 48.2103, 16.3614),
(27, 'Donauturm', 48.2403, 16.41),
(28, 'Gloriette', 48.1783, 16.3087),
(29, 'Hofburg', 48.2066, 16.3649),
(30, 'Islamisches Zentrum Wien / Moschee', 48.2451, 16.3978),
(31, 'Karlskirche', 48.1983, 16.3719),
(32, 'Musikverein', 48.2005, 16.3726),
(33, 'Secession', 48.2005, 16.3658),
(34, 'Sigmund Freud Museum', 48.2187, 16.363),
(35, 'Technisches Museum', 48.1909, 16.3179),
(36, 'Wien Museum Karlsplatz', 48.1993, 16.373),
(37, 'Wagner:Werk Museum Postsparkasse', 48.21, 16.3811),
(38, 'Palais Epstein', 48.2066, 16.3596),
(39, 'Universität Wien', 48.213, 16.3607),
(40, 'Unteres Belvedere', 48.197, 16.3798),
(41, 'Wotrubakirche', 48.1476, 16.2534),
(42, 'Kirche am Steinhof', 48.2107, 16.2788),
(43, 'Michaelerkirche', 48.208, 16.3674),
(44, 'Palmenhaus', 48.1843, 16.3029),
(45, 'Riesenrad', 48.2166, 16.3959),
(46, 'Schloss Schönbrunn', 48.1857, 16.3127),
(47, 'Schmetterlinghaus', 48.2054, 16.3664),
(48, 'Schönbrunn - Irrgarten', 48.1824, 16.309),
(49, 'Stephansdom', 48.2085, 16.3731),
(50, 'Tiergarten Schönbrunn', 48.182, 16.3042),
(51, 'UNO-City', 48.2348, 16.4168),
(52, 'Kunsthistorisches Museum', 48.2037, 16.3618),
(53, 'Leopold Museum', 48.2026, 16.359),
(54, 'MUMOK Museum Moderner Kunst Stiftung Ludwig Wien', 48.2038, 16.3578),
(55, 'Naturhistorisches Museum', 48.2052, 16.3598),
(56, 'Schatzkammer', 48.2068, 16.3655),
(57, 'Sisi Museum', 48.2077, 16.3656),
(58, 'Votivkirche', 48.2154, 16.3593),
(59, 'Hermesvilla Wien Museum', 48.1711, 16.2454),
(60, 'Otto Wagner Pavillon', 48.2003, 16.3703),
(61, 'Otto Wagner-Stadtpavillon Hietzing', 48.1875, 16.306),
(62, 'Schlosspark Schönbrunn', 48.1822, 16.3109),
(63, 'Wagenburg Schönbrunn', 48.1862, 16.3098);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `places`
--
ALTER TABLE `places`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `places`
--
ALTER TABLE `places`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
