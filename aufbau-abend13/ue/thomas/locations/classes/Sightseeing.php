<?php

class Sightseeing
{

    public array $locations;

    private function db()
    {
        $conn = new PDO('mysql:host='.DB['host'].';dbname='.DB['database'], DB['user'], DB['pwd']);
        return $conn;
    }

    public function read()
    {
        foreach ($this->db()->query('SELECT * FROM places') as $feature)
        {
            $lon = $feature['lon'];
            $lat = $feature['lat'];
            $name = $feature['ort'];
            $this->locations[] = new Location($name, $lat, $lon);
        }
    }
    
}
