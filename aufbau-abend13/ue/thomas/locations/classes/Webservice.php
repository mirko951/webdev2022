<?php
// https://www.php.net/manual/de/language.oop5.abstract.php
// Abstrakt definierte Klassen können nicht instantiiert werden.
// Abstrakte Klassen sind sozusagen nicht "fertig" - da fehlst meist eben eine oder mehere Implementierungen
// die in den Kindsklassen umgesetzt werden - je nach deren Spezialisierungen...
abstract class Webservice
{
    protected string $url;
    // TODO: später statt einem Array: Implementierung eines iterable Objects
    public array $locations;

    public function __construct(string $url)
    {
        $this->url = $url;
        $this->locations = [];
    }

    // todo für die kinder:
    // vervollständige die funktionalität durch die "schnittstelle" read...
    // diese methode soll genau so definiert sein:
    abstract public function read(?string $filter = null);

}
