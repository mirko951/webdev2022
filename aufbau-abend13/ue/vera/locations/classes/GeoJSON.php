<?php
/**
 * The class handles all GeoJSON input data
 */
class GeoJSON extends Webservice
{
    public function read(?string $filter = null)
    {
        // TODO: exception handling, falls die API nicht erreichbar
        $json = json_decode(file_get_contents($this->url), true);

        foreach ($json['features'] as $feature) {
            if (!isset($filter) || strpos($feature['properties']['NAME'], $filter) !== false) {
                $lon = $feature['geometry']['coordinates'][0];
                $lat = $feature['geometry']['coordinates'][1];
                if (isset($feature['properties']['BEZEICHNUNG'])){
                    $name = $feature['properties']['BEZEICHNUNG'];
                } else {
                    $name = $feature['properties']['NAME'];
                }
                $this->locations[] = new Location($name, $lat, $lon);
            }
        }
    }
}
