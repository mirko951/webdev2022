<?php
/**
 * The class handles all KML input data _const API_PHARM
 */
class KML extends Webservice
{
    public function read(?string $filter = null)
    {
        $kmldata = simplexml_load_file($this->url) or die("Error, konnte nicht geladen werden");

        foreach($kmldata->Document->Folder->Placemark as $kmldatum){
            if (!isset($filter) || strpos($kmldatum->description, $filter) !== false) {

                foreach($kmldatum->description as $kmldataName)
                    {
                        $kmldataString = htmlspecialchars_decode($kmldataName);
                        $lines = explode("\n", $kmldataString);
                        $name = strip_tags($lines[2]);
                    }
                foreach($kmldatum->LookAt->longitude as $kmllong){
                    $kmldataString = htmlspecialchars_decode($kmllong);
                    $lon = strip_tags($kmldataString);
                } 
                foreach($kmldatum->LookAt->latitude as $kmlat){
                    $kmldataString = htmlspecialchars_decode($kmlat);
                    $lat = strip_tags($kmldataString);
                } 

            $this->locations[] = new Location($name, $lat, $lon);
            }
        }
    }
}
