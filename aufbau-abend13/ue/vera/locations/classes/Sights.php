<?php
/*die Sights aus der Datenbank sightseeing.sql*/ 


class Sights
{   public array $locations;
    public $db;
    
    public function __construct()
    {
        // Verbindung zur Datenbank: 
        $this->db = new PDO('mysql:host=localhost;dbname=sightseeing', 'root', '', [ 
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
        ]);
       
        $this->locations = [];
    }
   
    public function read(?string $filter = null)
    {   $dbData = 'SELECT * FROM places';
        foreach ($this->db->query($dbData) as $row) {
            if (!isset($filter) || strpos($row['ort'], $filter) !== false) {
                $name = $row['ort'];
                $lon = $row['lon'];
                $lat = $row['lat'];
            
        $this->locations[] = new Location($name, $lat, $lon);
        }
        }
    }
}