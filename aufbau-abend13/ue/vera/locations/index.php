<?php
require_once 'inc/config.php';

$source = $_GET['source'] ?? 'driving';
switch ($source) {
    case 'driving':
        $data = new GeoJSON(API_DRIVING);
        break;
    case 'medical':
        $data = new GeoJSON(API_MEDICAL);
        break;
    // case 'hospital':
    //     $data = new GeoJSON(API_HOSP);
    //     break;
    case 'hospital_kml':
        $data = new KML(API_HOSP_KML);
        break;
    case 'church':
        $data = new CSV(API_CHURCH);
        break;
    case 'sights':
        $data = new Sights();
        break;
    default:
        $data = new GeoJSON(API_DRIVING);
        break;
}

$data->read();

include 'templates/header.php';

echo '<table class="table">';
foreach ($data->locations as $location) {
    echo '<tr>';
    echo '<td>' . $location->name . '</td>';
    echo '<td>' . $location->lat . '</td>';
    echo '<td>' . $location->lon . '</td>';
    echo '</tr>';
}
echo '</table>';

include 'templates/footer.php';
