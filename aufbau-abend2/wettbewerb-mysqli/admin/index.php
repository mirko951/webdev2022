<?php
require_once '../inc/config.inc.php';
// Session starten: 1. prüft, ob der Browser im request-header einen session cookie (default name: "PHPSESSID") mitgeliefert hat,
// wenn ja: prüft, ob am !!! Server Daten zu diesem Session-Hashwert (der im Cookie steht) existieren...
// falls ja, werden diese Daten in das $_SESSION Superglobal gespeichert!

// TODO: auslagern der Überprüfung des Login Status in eine eigene Funktion...
session_start();
if (!isset($_SESSION['login']) || $_SESSION['login'] !== 'ok') {
    // https://www.php.net/manual/de/function.header.php
    header('location: login.php');
    // Wichtig: immer nach einem Umleitungsheader: Programmende! (falls die Umleitung fehlschlägt, oder falls jemand über cURL oder ähnliches den Request ausführt)
    exit;
}
include '../templates/header.php';

$db = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

$sql = 'SELECT * FROM registrations ORDER BY lastname';

$result = mysqli_query($db, $sql);
echo '<table>';
while ($row = mysqli_fetch_assoc($result)) {
    echo "<tr>";
    foreach ($row as $col) {
        echo "<td>$col</td>";
    }
    echo "</tr>";
}
echo '</table>';

include '../templates/footer.php';
