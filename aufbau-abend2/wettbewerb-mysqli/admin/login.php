<?php
require_once '../inc/config.inc.php';

// Wenn über POST Request, dann Login prüfen und umleiten
if (count($_POST) > 0) {
    session_start();
    // Passwörter sollten verschlüsselt gespeichert werden!
    if ($_POST['user'] === 'admin' && password_verify($_POST['password'], '$2y$10$c796E2F61SBXiA7KXOuHieKtKMQxFiBltUx0NfauwUrGQlrMNWfxm')) {
        $_SESSION['login'] = 'ok';
        $_SESSION['userid'] = '23423423';
        header('location: index.php');
        exit;
    }
}

include '../templates/header.php';
?>
<form action="login.php" method="post">
    User: <input type="text" name="user"><br>
    Password: <input type="password" name="password"><br>
    <input type="submit" value="Login">
</form>
<?php
include '../templates/footer.php';
