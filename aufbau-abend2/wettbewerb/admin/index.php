<?php
require_once '../inc/config.inc.php';
protectSite();
include '../templates/header.php';

// TODO: option: result als array oder objekt...
$db = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME, DB_USER, DB_PASSWORD, [
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
]);

$sql = 'SELECT * FROM registrations ORDER BY lastname';

// TODO: lange Weg: result, fetch, schleife...

// kurze Weg:
echo "<table>";
foreach ($db->query($sql) as $row) {
    echo "<tr>";
    foreach ($row as $col) {
        echo "<td>$col</td>";
    }
    echo "</tr>";
}
echo "</table>";

// längerer Weg:
echo "<table>";
$result = $db->query($sql);
while ($row = $result->fetch()) {
    echo "<tr>";
    foreach ($row as $col) {
        echo "<td>$col</td>";
    }
    echo "</tr>";
}
echo "</table>";

include '../templates/footer.php';
