SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";
CREATE DATABASE IF NOT EXISTS `minis` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE `minis`;

CREATE TABLE `bundesliga` (
  `id` int(11) NOT NULL,
  `verein` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `punkte` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `bundesliga` (`id`, `verein`, `punkte`) VALUES
(1, 'RB Salzburg', 3),
(2, 'Sportklub', 90),
(3, 'Vienna', 80),
(4, 'Austria Wien', 1),
(5, 'SK Rapid', 100),
(6, 'Mattersburg', 24),
(7, 'LASK', 30),
(8, 'Sturm Graz', 41),
(9, 'St. Pölten', 12),
(10, 'SV Horn', 32);

CREATE TABLE `modelle` (
  `id` int(11) NOT NULL,
  `modell` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `farbe` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `modelle` (`id`, `modell`, `farbe`) VALUES
(1, 'VW T3 JX', 'grün'),
(2, 'VW T3 JX', 'gelb'),
(3, 'VW T3 JX', 'rot'),
(4, 'VW T3 Pritsche', 'gelb'),
(5, 'VW T3 Pritsche', 'blau'),
(6, 'VW T3 Pritsche', 'rot'),
(7, 'VW T3 Pritsche', 'schwarz'),
(8, 'Maserati Biturbo Spyder', 'gelb'),
(9, 'Maserati Biturbo Spyder', 'schwarz'),
(10, 'Maserati Biturbo Spyder', 'rot');

CREATE TABLE `role` (
  `rid` int(11) NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `role` (`rid`, `role`) VALUES
(1, 'admin'),
(2, 'redakteur'),
(3, 'editor'),
(4, 'guest');

CREATE TABLE `user` (
  `uid` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `user` (`uid`, `username`, `role_id`) VALUES
(1, 'sepp', 4),
(2, 'gabi', 1),
(3, 'uschi', 2),
(4, 'johnny', 8),
(5, 'kurtl', 3),
(6, 'ferdl', 1),
(7, 'trude', 2),
(8, 'hansi', 4),
(9, 'mimi', 2),
(10, 'rüdiger', 5);

CREATE TABLE `zufall` (
  `id` int(11) NOT NULL,
  `wert` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


ALTER TABLE `bundesliga`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `modelle`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `role`
  ADD PRIMARY KEY (`rid`);

ALTER TABLE `user`
  ADD PRIMARY KEY (`uid`);

ALTER TABLE `zufall`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `bundesliga`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

ALTER TABLE `modelle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

ALTER TABLE `role`
  MODIFY `rid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

ALTER TABLE `user`
  MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

ALTER TABLE `zufall`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;
