<!doctype html>
<html>
  <head>
  <title>SQL Minis</title>
    <meta charset="utf-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <style>
      table.bl tr:nth-child(-n+4) {
        background-color: yellow;
      }
      table.bl tr:first-child {
        background-color: white;
      }
      table.bl tr:last-child {
        background-color: red;
      }
    </style>
    </head>
  <body>

  <table class="table table-striped table-hover bl">

    <tr>
      <th>Id</th>
      <th>Verein</th>
      <th>punkte</th>
    </tr>

  
  <?php
    /* Aufgabe (betroffene Tabellen: `bundesliga`)

     * gib die Tabelle `bundesliga` in Form einer HTML-Tabelle aus (sortiert nach `punkte` absteigend), und setze den Hintergrund der 3 ersten Zeilen auf gelb, und der letzten Zeile auf rot

     */
    $db = new PDO('mysql:host=localhost;dbname=minis', 'root', '');
    $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    foreach ($db->query('SELECT * FROM bundesliga ORDER BY punkte DESC') as $row) {
      ?>
        <tr>
          <td><?= $row['id'] ?></td>
          <td><?= $row['verein'] ?></td>
          <td><?= $row['punkte'] ?></td>
        </tr>
      <?php
      }
      ?>
  </body>
</html>
