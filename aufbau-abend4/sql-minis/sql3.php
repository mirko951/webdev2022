<!doctype html>
<html>
  <head>
    <title>SQL Minis</title>
    <meta charset="utf-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  </head>
  <body>
  <table class="table table-striped table-hover">

<tr>
  <th>Id</th>
  <th>Verein</th>
  <th>punkte</th>
</tr>

    <?php
    /* Aufgabe (betroffene Tabellen: `bundesliga`)

     * gib alle Einträge der Tabelle `bundesliga` aus, dessen Feld `verein` mit einem "S" beginnt (1. Zeichen)

     */
    $db = new PDO('mysql:host=localhost;dbname=minis', 'root', '');
    $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    foreach ($db->query('SELECT * FROM bundesliga WHERE verein LIKE "s%"') as $row) {
      ?>
        <tr>
          <td><?= $row['id'] ?></td>
          <td><?= $row['verein'] ?></td>
          <td><?= $row['punkte'] ?></td>
        </tr>
      <?php
      }
      ?>
  </body>
</html>
