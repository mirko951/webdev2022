<!doctype html>
<html>
  <head>
    <title>SQL Minis</title>
    <meta charset="utf-8">
  </head>
  <body>
    <?php
    /* Aufgabe (betroffene Tabellen: `zufall`)

     * füge durch eine Schleife 100 Datensätze in die Tabelle `zufall` ein (INSERT) und setze dort die Spalte `wert` jeweils auf eine zufällige Zahl zwischen 0 und 10000
     * 2 Möglichkeiten: entweder mit php: rand(), oder direkt in der query mit RAND()

     */
    $db = new PDO('mysql:host=localhost;dbname=minis', 'root', '');
    $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

    $stmt = $db->prepare('INSERT INTO zufall (wert) VALUES (?)');
    for ($i = 0; $i < 100; ++$i) {
      $stmt->execute([ rand(0, 10000) ]);
    }

    /*
    for ($i = 0; $i < 100; ++$i) {
      $db->query('INSERT INTO zufall (wert) VALUES (ROUND(RAND()*10000))');
    }
    */
    ?>
  </body>
</html>
