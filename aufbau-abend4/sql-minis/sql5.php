<!doctype html>
<html>
  <head>
    <title>SQL Minis</title>
    <meta charset="utf-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  </head>
  <body>
  <table class="table table-striped table-hover">

<tr>
  <th>Id</th>
  <th>Modell</th>
  <th>Farbe</th>
</tr>
    <?php
    /* Aufgabe (betroffene Tabellen: `modelle`)

     * gib alle Datensätze aus der Tabelle `modelle` aus, dessen Feld `modell` den Teilstring "T3" beinhaltet

     */
    $db = new PDO('mysql:host=localhost;dbname=minis', 'root', '');
    $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    foreach ($db->query('SELECT * FROM modelle WHERE modell LIKE "%t3%"') as $row) {
      ?>
        <tr>
          <td><?= $row['id'] ?></td>
          <td><?= $row['modell'] ?></td>
          <td><?= $row['farbe'] ?></td>
        </tr>
      <?php
      }
    ?>
    </table>
  </body>
</html>
