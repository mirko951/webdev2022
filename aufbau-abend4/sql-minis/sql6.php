<!doctype html>
<html>
  <head>
    <title>SQL Minis</title>
    <meta charset="utf-8">
  </head>
  <body>

    <?php
    $deleted_colors = [ "gelb", "rot", "blau", "schwarz" ];

    /* Aufgabe (betroffene Tabellen: `modelle`)

     * lösche (DELETE) alle Datensätze aus Tabelle `modelle`, die in der Spalte `farbe` einen Wert eingetragen haben, welche in dem Array $deleted_colors stehen!

     */
    $db = new PDO('mysql:host=localhost;dbname=minis', 'root', '');
    $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

    $mask_array = [];
    for ($i = 0; $i < count($deleted_colors); ++$i) {
      $mask_array[] = '?';
    }
    $mask = implode(", ", $mask_array);

    /*
    $mask = '';
    for ($i = 0; $i < count($deleted_colors); ++$i) {
      if ($i === 0) {
        $mask .= '?';
      } else {
        $mask .= ', ?';
      }
    }
    */

    $stmt = $db->prepare('DELETE FROM modelle WHERE farbe IN (' . $mask . ')');
    $stmt->execute($deleted_colors);
    ?>
  </body>
</html>
