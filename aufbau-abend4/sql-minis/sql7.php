<!doctype html>
<html>
  <head>
    <title>SQL Minis</title>
    <meta charset="utf-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

  </head>
  <body>
  <table class="table table-striped table-hover">

<tr>
  <th>User Id</th>
  <th>User</th>
  <th>Role Id</th>
  <th>Role</th>
</tr>
    <?php
    /* Aufgabe (betroffene Tabellen: `user`, `role`)

     * gib alle user aus - und daneben die role des users (auch, falls keine Rolle mit der ID existiert) (Hinweis: LEFT JOIN / role_id = rid)

     */
    function dbConnect() {
      $db = new PDO('mysql:host=localhost;dbname=minis', 'root', '');
      $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
      return $db;
    }

    $db = dbConnect();

    foreach ($db->query('SELECT * FROM user LEFT JOIN role ON rid = role_id') as $row) {
      echo '<tr>';
      echo '<td>' . $row['uid'] . '</td>';
      echo '<td>' . $row['username'] . '</td>';
      echo '<td>' . $row['rid'] . '</td>';
      echo '<td>' . $row['role'] . '</td>';
      echo '</tr>';
    }

    /**
     * Schlechtes Beispiel(!!!)
     * Versuche SQL Queries innerhalb von Schleifen, die nicht notwendig sind, zu vermeiden!
     * Vorallem: wenn möglich, dann JOIN und nicht einzeln abfragen...
     */
    foreach ($db->query('SELECT * FROM user') as $row) {
      echo '<tr>';
      echo '<td>' . $row['uid'] . '</td>';
      echo '<td>' . $row['username'] . '</td>';
      $stmt = $db->prepare('SELECT * FROM role WHERE rid = ?');
      $stmt->execute([ $row['role_id'] ]);
      if ($role = $stmt->fetch()) {
        echo '<td>' . $role['rid'] . '</td>';
        echo '<td>' . $role['role'] . '</td>';
      }
      echo '</tr>';
    }
    ?>
  </body>
</html>
