<!doctype html>
<html>
  <head>
    <title>SQL Minis</title>
    <meta charset="utf-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

  </head>
  <body>
  <table class="table table-striped table-hover">
  <tr>

  <th>Role</th>
  <th>User</th>
</tr>
    <?php
    /* Aufgabe (betroffene Tabellen: `user`, `role`)

     * gib alle rolen-namen aus - und daneben die anzahl der user, die diese role haben (Hinweis: COUNT() und GROUP BY)

     */
    $db = new PDO('mysql:host=localhost;dbname=minis', 'root', '');
    $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    foreach ($db->query('SELECT role, COUNT(uid) AS users FROM user LEFT JOIN role ON role_id = rid GROUP BY rid') as $row) {
      echo '<tr>';
      echo '<td>' . $row['role'] . '</td>';
      echo '<td>' . $row['users'] . '</td>';
      echo '</tr>';
    }
    ?>
    </table>
  </body>
</html>
