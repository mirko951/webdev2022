-- phpMyAdmin SQL Dump
-- version 5.1.4deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Erstellungszeit: 25. Jan 2023 um 21:34
-- Server-Version: 10.6.11-MariaDB-2
-- PHP-Version: 8.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `contest`
--
CREATE DATABASE IF NOT EXISTS `contest` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `contest`;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `registrations`
--

CREATE TABLE `registrations` (
  `id` mediumint(9) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `street` varchar(255) NOT NULL,
  `postal` varchar(15) NOT NULL,
  `city` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Daten für Tabelle `registrations`
--

INSERT INTO `registrations` (`id`, `firstname`, `lastname`, `street`, `postal`, `city`, `country`, `image`) VALUES
(2, 'Markus', 'Huber', 'Teststraße', '12345', 'Vienna', 'AT', 'testbild'),
(5, 'asdf', 'Muster', 'asf', '12345', 'asdf', 'AT', 'uploads/63c9974a41403.jpg'),
(10, '2023-01-20 21:45:39', 'aaaa', 'x', 'y', 'z', 'a', 'b'),
(11, 'a', 'a', 'a', 'a', 'a', 'a', ''),
(12, '2023-01-20 21:49:24', 'a', 'a', 'a', 'a', 'a', ''),
(13, 'asdklfjasdlkf', 'asifjaslkd', 'aslfkjsadlkf', '2222', 'asldfjsdlfk', 'AT', '');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `users`
--

CREATE TABLE `users` (
  `uid` tinyint(3) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `pwd` varchar(255) NOT NULL,
  `last_login` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Daten für Tabelle `users`
--

INSERT INTO `users` (`uid`, `name`, `pwd`, `last_login`) VALUES
(1, 'admin', '$2y$10$c796E2F61SBXiA7KXOuHieKtKMQxFiBltUx0NfauwUrGQlrMNWfxm', '2023-01-25 19:21:23'),
(2, 'test', '$2y$10$uKficuZXSQzaq/Kh1BarSupuPQF4aKTKmsuiH8WkTjJTYs.I8.ad6', '2023-01-25 19:21:23');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `registrations`
--
ALTER TABLE `registrations`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`uid`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `registrations`
--
ALTER TABLE `registrations`
  MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT für Tabelle `users`
--
ALTER TABLE `users`
  MODIFY `uid` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
