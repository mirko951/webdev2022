-- phpMyAdmin SQL Dump
-- version 5.1.4deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Erstellungszeit: 25. Jan 2023 um 21:34
-- Server-Version: 10.6.11-MariaDB-2
-- PHP-Version: 8.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `ma_verwaltung`
--
CREATE DATABASE IF NOT EXISTS `ma_verwaltung` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `ma_verwaltung`;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `abteilungen`
--

CREATE TABLE `abteilungen` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Daten für Tabelle `abteilungen`
--

INSERT INTO `abteilungen` (`id`, `name`) VALUES
(1, 'Entwicklung'),
(2, 'Grafik'),
(3, 'Projektleitung');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `personal`
--

CREATE TABLE `personal` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `vorname` varchar(255) NOT NULL,
  `nachname` varchar(255) NOT NULL,
  `svn` char(4) NOT NULL DEFAULT '',
  `geburtsdatum` date DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp(),
  `abt_id` smallint(5) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Daten für Tabelle `personal`
--

INSERT INTO `personal` (`id`, `vorname`, `nachname`, `svn`, `geburtsdatum`, `created`, `abt_id`) VALUES
(1, 'Markus', 'test', '', '2020-01-01', '2023-01-25 18:19:04', 1),
(2, 'test', 'test', '', NULL, '2023-01-25 18:24:35', 2),
(3, 'Jane', 'Doe', '1234', NULL, '2023-01-25 19:34:22', 1),
(4, 'John', 'Doe', '1234', NULL, '2023-01-25 19:34:22', 2);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `personal_skills`
--

CREATE TABLE `personal_skills` (
  `pid` smallint(5) UNSIGNED NOT NULL,
  `sid` smallint(5) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Daten für Tabelle `personal_skills`
--

INSERT INTO `personal_skills` (`pid`, `sid`) VALUES
(1, 1),
(1, 2),
(1, 4),
(2, 4),
(3, 1),
(3, 3),
(4, 3);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `skills`
--

CREATE TABLE `skills` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Daten für Tabelle `skills`
--

INSERT INTO `skills` (`id`, `name`) VALUES
(1, 'PHP'),
(2, 'Datenbanken'),
(3, 'CSS'),
(4, 'JavaScript');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `abteilungen`
--
ALTER TABLE `abteilungen`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `personal`
--
ALTER TABLE `personal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `abt_id` (`abt_id`);

--
-- Indizes für die Tabelle `personal_skills`
--
ALTER TABLE `personal_skills`
  ADD PRIMARY KEY (`pid`,`sid`),
  ADD KEY `pid` (`pid`),
  ADD KEY `sid` (`sid`);

--
-- Indizes für die Tabelle `skills`
--
ALTER TABLE `skills`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `abteilungen`
--
ALTER TABLE `abteilungen`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT für Tabelle `personal`
--
ALTER TABLE `personal`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT für Tabelle `skills`
--
ALTER TABLE `skills`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `personal`
--
ALTER TABLE `personal`
  ADD CONSTRAINT `personal_ibfk_1` FOREIGN KEY (`abt_id`) REFERENCES `abteilungen` (`id`);

--
-- Constraints der Tabelle `personal_skills`
--
ALTER TABLE `personal_skills`
  ADD CONSTRAINT `personal_skills_ibfk_1` FOREIGN KEY (`sid`) REFERENCES `skills` (`id`),
  ADD CONSTRAINT `personal_skills_ibfk_2` FOREIGN KEY (`pid`) REFERENCES `personal` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
