<?php
require_once '../inc/config.inc.php';
protectSite();
include '../templates/header.php';

$db = dbConnect();

echo 'Hallo ' . $_SESSION['username'] . '<hr>';

// SQL Abfrage aller Registrierungen...
$sql = 'SELECT * FROM registrations ORDER BY lastname';

echo "<table>";
// Für jede Ergebniszeile (Datensatz) der Abfrage, füge eine HTML-Zeile hinzu...
foreach ($db->query($sql) as $row) {
    echo "<tr>";
    // Für jede Spalte (einer Zeile), füge eine HTML-Spalte hinzu...
    foreach ($row as $col) {
        echo "<td>$col</td>";
    }
    echo "</tr>";
}
echo "</table>";

include '../templates/footer.php';
