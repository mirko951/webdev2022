<?php
require_once '../inc/config.inc.php';

// Wenn über POST Request, dann Login prüfen und umleiten
if (count($_POST) > 0) {
    session_start();
    // Passwörter sollten verschlüsselt gespeichert werden!
    // SELECT * FROM users WHERE name = $_POST['username']
    $db = dbConnect();
    $stmt = $db->prepare('SELECT * FROM users WHERE name = ?');
    // SELECT * FROM user JOIN role ON user.role_id = role.rid WHERE role = "admin" AND username = ?
    $stmt->execute([ $_POST['user'] ]);
    if ($row = $stmt->fetch()) {
        if (password_verify($_POST['password'], $row['pwd'])) {
            $db->query('UPDATE users SET last_login = NOW()');
            $_SESSION['login'] = 'ok';
            $_SESSION['userid'] = $row['uid'];
            $_SESSION['username'] = $row['name'];
            header('location: index.php');
            exit;
        }
    }
}

include '../templates/header.php';
?>
<form action="login.php" method="post">
    User: <input type="text" name="user"><br>
    Password: <input type="password" name="password"><br>
    <input type="submit" value="Login">
</form>
<?php
include '../templates/footer.php';
