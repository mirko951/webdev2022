<?php
/**
 * This function validates all input fields and returns an array of the error-messages.
 * If no errors are found, an empty array is returned.
 * 
 * @return array An array of error messages or an empty array if no errors.
 */
function validateRegisterForm() : array
{
    $errors = [];
    if (empty(trim($_POST['firstname']))) {
        $errors[] = "Bitte geben Sie einen Vornamen ein!";
    
    }
    if (empty(trim($_POST['lastname']))) {
        $errors[] = "Bitte geben Sie einen Nachnamen ein!";
    
    }
    if (empty(trim($_POST['address']))) {
        $errors[] = "Bitte geben Sie eine Adresse ein!";
    
    }
    if (empty(trim($_POST['postal']))) {
        $errors[] = "Bitte geben Sie eine Postleitzahl ein!";
    
    } else {
        $len = strlen(trim($_POST['postal']));
        if ($len < 4 || $len > 5 || !is_numeric($_POST['postal'])) {
            $errors[] = "Bitte geben Sie eine 4-5 stellige gültige Postleitzahl ein!";
        }
    }
    if (empty(trim($_POST['city']))) {
        $errors[] = "Bitte geben Sie einen Ort ein!";
    
    }
    if (empty(trim($_POST['country']))) {
        $errors[] = "Bitte geben Sie ein Land ein!";
    }

    if (!empty($_FILES['image']['name'])) {
        $allowed_filetypes = ['jpg', 'jpeg', 'png', 'gif', 'pdf'];
        $filetype = strtolower(pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION));
        if (!in_array($filetype, $allowed_filetypes)) {
            $errors[] = "Bitte laden Sie nur ein Bild hoch (jpg, png, gif, pdf)!";
        } else {
            // Limit to 5MB
            if ($_FILES['image']['size'] > 5242880) {
                $errors[] = "Max. 5MB!";
            }
        }
    }

    return $errors;
}

/**
 * Uploads the file and saves the registration into the database.
 */
function saveRegistration()
{
    $target = '';
    if (!empty($_FILES['image']['name'])) {
        $filetype = strtolower(pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION));
        $filename = uniqid() . '.' . $filetype;
        $target = FILE_UPLOADS . $filename;
        move_uploaded_file($_FILES['image']['tmp_name'], $target);
    }

    // DB-Operationen:
    // 1. Connection zur DB!
    $db = dbConnect();

    // $fieldlist ist zwar nicht notwendig ($_POST ginge auch), aber falls sich die Reihenfolge im Formular ändert, oder neue Eingabefelder hinzukommen, vermeide ich damit einen Fehler in der CSV Fomartierung:
    $fieldlist = [
        $_POST['firstname'],
        $_POST['lastname'],
        $_POST['address'],
        $_POST['postal'],
        $_POST['city'],
        $_POST['country'],
        $target
    ];

    // 2. Einfügen in Tabelle:
    // NIEMALS variable Werte (schon gar nicht Usereingaben) DIREKT in SQL-Queries einbauen!!!
    // https://de.wikipedia.org/wiki/SQL-Injection
    // https://en.wikipedia.org/wiki/SQL_injection
    /*
    $sql = 'INSERT INTO registrations (firstname, `lastname`, `street`, `postal`, `city`, `country`, `image`)
            VALUES ("' . $_POST['firstname'] . '", "' . $_POST['lastname'] . '", "' . $_POST['address'] . '", "' . $_POST['postal'] . '", "' . $_POST['city'] . '", "' . $_POST['country'] . '", "' . $target . '")';
    $db->query($sql);
    */
    $sql = 'INSERT INTO registrations (firstname, `lastname`, `street`, `postal`, `city`, `country`, `image`)
            VALUES (?, ?, ?, ?, ?, ?, ?)';
    $stmt = $db->prepare($sql);
    $stmt->execute($fieldlist);

    // 3. Schließen der DB-Verbindung
    $db = null;
}

/**
 * Checks whether the admin is logged in. If not, redirect to the login page.
 */
function protectSite() {
    session_start();
    if (!isset($_SESSION['login']) || $_SESSION['login'] !== 'ok') {
        // https://www.php.net/manual/de/function.header.php
        header('location: login.php');
        // Wichtig: immer nach einem Umleitungsheader: Programmende! (falls die Umleitung fehlschlägt, oder falls jemand über cURL oder ähnliches den Request ausführt)
        exit;
    }
}

/**
 * Create connection to the database
 * 
 * @return PDO the connection class instance
 */
function dbConnect() : PDO {
    $db = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME, DB_USER, DB_PASSWORD, [
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
    ]);
    return $db;
}
