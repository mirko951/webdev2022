<?php
/**
 * TODO: Description
 *
 * TODO: Eigenschaften auflisten, etc.
 */
class Staff
{
    public string $firstname;
    public string $lastname;
    private string $email;
    public int $income;

    /**
     * Wenn eine Eigenschaft private (oder protected) ist, dann kann darauf nur INNERHALB der Klasse,
     * d.h. genauer: innerhalb einer Methode, die in eben dieser Klasse definiert ist, zugegriffen werden.
     * (Bzw. bei protected ist einer verebten Methode)
     *
     * Wozu das ganze? Bspw. möchten wir verhindern, dass jemand eine ungültige Mail Adresse als Objekteigenschaft setzt.
     * Daher: 1. email als private, 2. public setter Methode, die die Mail Adresse validiert und dann setzt.
     *
     * @param string $email the email
     * @return boolean true if the email is valid and set
     */
    public function setEmail(string $email) : bool {
        if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            return false;
        }
        $this->email = $email;
        return true;
    }

    /**
     * Getter Methode: da die Eigenschaft "email" private ist, brauche ich auch eine s.g. "Getter" Methode,
     * sofern ich diese Eigenschaft auslesen will... D.h. eine Getter Methode liefert (return) die private property...
     */
    public function getEmail() {
        return $this->email;
    }

}

// TODO: auf einer Seite, auf der MA gespeichert werden:
$ma1 = new Staff;
$ma1->firstname = 'Markus';
$ma1->lastname = 'Huber';
$ma1->setEmail('test@test.at');
// $ma1->save();


echo $ma1->getEmail();


// TODO: mail validierung
// $ma1->email = 'asdfasdf';

// echo $ma1->email;
$ma1->income = '5';

// var_dump($ma1);
