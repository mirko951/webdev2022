<?php
/**
 * Hinweis: das ist ein Anschauungsbeispiel, das nicht sauber trennt zwischen Ausgabe-Logik, Programmlogik, der Datenbankschnittstelle...
 * (Wie es bspw. auch Frameworks voraussetzen, dort wird meist nach dem MVC Schema getrennt. - Dazu im letzten Modul.)
 */
class Staff
{
    private int $id;
    private string $firstname;
    private string $lastname;
    private string $svn;
    private string $birthdate;
    public bool $isValid = false;

    public function set(string $firstname, string $lastname, string $svn, string $birthdate) : bool {
        $this->validateData($firstname, $lastname, $svn, $birthdate);
        if ($this->isValid) {
            $this->firstname = $firstname;
            $this->lastname = $lastname;
            $this->svn = $svn;
            $this->birthdate = $birthdate;
            return true;
        }
        return false;
    }

    private function validateData(string $firstname, string $lastname, string $svn, string $birthdate) : bool {
        if (mb_strlen(trim($firstname)) < 2) {
            $this->isValid = false;
            return false;
        }
        if (mb_strlen(trim($lastname)) < 2) {
            $this->isValid = false;
            return false;
        }
        if (mb_strlen(trim($svn)) !== 4 || !is_numeric($svn)) {
            $this->isValid = false;
            return false;
        }
        // TODO: datumsvalidation
        $this->isValid = true;
        return true;
    }

    public function save() {
        $db = new PDO('mysql:host=localhost;dbname=ma_verwaltung', 'root', '');
        $stmt = $db->prepare('INSERT INTO personal (vorname, nachname, svn, geburtsdatum, abt_id) VALUES (?, ?, ?, ?, 1)');
        $stmt->execute([
            $this->firstname,
            $this->lastname,
            $this->svn,
            $this->birthdate
        ]);
    }
}
