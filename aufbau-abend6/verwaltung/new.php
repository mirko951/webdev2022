<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <title>Übersicht</title>
</head>
<body>
    <div id="container">
        <div id="row">
            <a href="./new.php" class="btn btn-primary">Neuer Eintrag</a>
        </div>
        <div id="row">
            <form method="POST" action="save.php">
                Vorname: <input type="text" name="firstname"><br>
                Nachname: <input type="text" name="lastname"><br>
                SVN: <input type="text" name="svn"><br>
                Geburtsdatum: <input type="text" name="birthdate"><br>
                <input type="submit" value="Speichern">
            </form>
        </div>
    </div>
</body>
</html>
