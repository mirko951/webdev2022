<?php
function myLoader(string $classname)
{
    require_once "classes/$classname.php";
}
spl_autoload_register('myLoader');

$datenquelle = 'https://data.wien.gv.at/daten/geo?service=WFS&request=GetFeature&version=1.1.0&typeName=ogdwien:FAHRSCHULEOGD&srsName=EPSG:4326&outputFormat=json';
if (isset($_GET['data']) && $_GET['data'] === 'docs') {
    $datenquelle = 'https://data.wien.gv.at/daten/geo?service=WFS&request=GetFeature&version=1.1.0&typeName=ogdwien:ARZTOGD&srsName=EPSG:4326&outputFormat=json';
}

$data = new GeoJSON($datenquelle);
$data->read();

// var_dump($data->locations);
// TODO: aufräumen...
?>
<a href='index.php?data=drivers'>Liste alle Fahrschulen</a><br>
<a href='index.php?data=docs'>Liste alle ÄrztInnen</a><br>
<?php

echo '<table>';
foreach ($data->locations as $location) {
    echo '<tr>';
    echo '<td>' . $location->name . '</td>';
    echo '<td>' . $location->lat . '</td>';
    echo '<td>' . $location->lon . '</td>';
    echo '</tr>';
}
echo '</table>';
