<?php
/**
 * The autoloader, for spl_autoload_register
 */
function myLoader(string $classname)
{
    require_once "classes/$classname.php";
}
