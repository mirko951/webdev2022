<?php
use Locations\Input\Sightseeing;
use Locations\Output\JSON;

require_once 'inc/config.php';

$data = new Sightseeing();
$data->read();
$output = new JSON($data);
$output->render();
