<?php
namespace Locations\Input;

/**
 * The class handles all CSV (Features) input data
 */
class CSV extends Webservice
{
    public function read(?string $filter = null)
    {
        // TODO: exception handling, falls die API nicht erreichbar
        $fp = fopen($this->url, 'r');

        if (fgetcsv($fp)) {
            while ($row = fgetcsv($fp)) {
                if (!isset($filter) || strpos($row[3], $filter) !== false) {
                    $points = explode(' ', $row[2]);
                    $lon = substr($points[1], 1);
                    $lat = substr($points[2], 0, -1);
                    $name = $row[3];
                    $this->locations[] = new \Locations\Location($name, $lat, $lon);
                }
            }
        }
    }
}
