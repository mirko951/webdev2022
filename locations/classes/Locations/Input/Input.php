<?php
namespace Locations\Input;

interface Input
{
    /**
     * Input algorhythm for the spezific input format
     */
    public function read(?string $filter = null);
}
