<?php
namespace Locations\Input;

class Sightseeing implements Input
{

    public array $locations;

    private function db()
    {
        $conn = new \PDO('mysql:host='.DB['host'].';dbname='.DB['database'], DB['user'], DB['pwd']);
        return $conn;
    }

    public function read(?string $filter = null)
    {
        foreach ($this->db()->query('SELECT * FROM places') as $feature)
        {
            $lon = $feature['lon'];
            $lat = $feature['lat'];
            $name = $feature['ort'];
            $this->locations[] = new \Locations\Location($name, $lat, $lon);
        }
    }
    
}
