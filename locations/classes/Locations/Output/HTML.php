<?php
namespace Locations\Output;

/**
 * This class handles the html output rendering...
 */
class HTML extends Output
{
    const HEADER = 'templates/header.php';
    const FOOTER = 'templates/footer.php';

    public function render()
    {
        include self::HEADER;
        echo $this->htmlTable();
        include self::FOOTER;
    }
}
