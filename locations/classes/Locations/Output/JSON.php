<?php
namespace Locations\Output;

/**
 * This class renders a JSON output of the Locations (for API usage).
 */
class JSON extends Output
{
    public function render()
    {
        header('Content-Type: application/json');
        echo json_encode( [ 'Locations' => $this->inputObject->locations ] );
    }
}
