<?php
namespace Locations\Output;

/**
 * Desc.!!! TODO
 */
abstract class Output
{
    protected \Locations\Input\Input $inputObject;

    public function __construct(\Locations\Input\Input $obj)
    {
        $this->inputObject = $obj;
    }

    protected function htmlTable(bool $htmlencode = true) : string
    {
        $html = '<table class="table">';
        foreach ($this->inputObject->locations as $location) {
            $html .= '<tr>';
            $html .= '<td>' . ($htmlencode ? htmlspecialchars($location->name) : $location->name) . '</td>';
            $html .= '<td>' . $location->lat . '</td>';
            $html .= '<td>' . $location->lon . '</td>';
            $html .= '</tr>';
        }
        $html .= '</table>';
        return $html;
    }

    abstract public function render();
}
