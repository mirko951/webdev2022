<?php
/**
 * The autoloader, for spl_autoload_register
 */
function myLoader(string $classname)
{
    $classname = str_replace('\\', '/', $classname);
    require_once "classes/$classname.php";
}
