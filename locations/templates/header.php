<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <title><?= APP_NAME ?></title>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1><?= APP_NAME ?></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <form method="GET">
                    <select name="source" class="form-control">
                        <option value="">Datenquelle</option>
                        <option value="driving">Driving</option>
                        <option value="medical">Medical</option>
                        <option value="church">Church</option>
                    </select>
                    <select name="dst" class="form-control">
                        <option value="">Ausgabe</option>
                        <option value="html">HTML</option>
                        <option value="pdf">PDF</option>
                        <option value="xls">XLS</option>
                        <option value="json">JSON</option>
                    </select>
                    <input type="submit" value="Submit" class="form-control">
                </form>
