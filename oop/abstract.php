<?php
/**
 * Abstrakte Klassen haben im Wesentlichen zwei Zwecke:
 * 1. von einer abstrakten Klasse kann KEIN Objekt instanziiert werden, in der Regel, weil die Klasse noch nicht "fertig" ist - es fehlt etwas.
 * Abstrakte Klassen sind also immer Klassen, die spezialisiert werden müssen: d.h. aus ihnen muss geerbt werden.
 * 2. gibt es die Möglichkeit, abstrakte Methoden zu deklarieren (dann muss die Klasse selbst auch abstract sein).
 * Um festzulegen, wie die "Signatur" der in einer Kindsklasse zu implementierenden Methode auszusehen hat.
 * Signatur: Name der Methode, Parameter, Sichtbarkeit, etc.
 * Signaturregeln: https://www.php.net/manual/de/language.oop5.basic.php#language.oop.lsp
 */
abstract class Staff
{
    public string $firstname;
    public string $lastname;
    protected string $email;

    public function setMail(string $mail)
    {
        // TODO: validieren
        $this->email = $mail;
    }

    abstract public function sendSalary();
}

class Freelancer extends Staff
{
    public float $fee;

    public function sendSalary()
    {
        // TODO:
        // Logik: lese aus DB Arbeitsstunden und fee aus letzten Monat
        $hours = 60;
        $this->fee = 80;
        $sum = $hours * $this->fee;
        // TODO: speichere $sum in Auszahlungstabelle
        echo "debug test 1";
    }
}

class Employee extends Staff
{
    public float $salary;

    public function sendSalary()
    {
        // TODO:
        // Logik: lese aus DB Gehalt
        $sum = $this->salary = 2500;
        // TODO: speichere $sum in Auszahlungstabelle
        echo "debug test 2";
    }
}

$test1 = new Freelancer;
$test1->sendSalary();

$test2 = new Employee;
$test2->sendSalary();
