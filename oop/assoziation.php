<?php

// Diese 3 Klassen stehen miteinander in Verbindung...
// Aber hier keine Generalisierung (durch Verebung), sondern sogenannte Assoziationen:
// d.h. ein Objekt der einen Klasse, enthält ein oder mehrere Objekte einer anderen Klasse.

class Staff
{
    public ?string $firstname;
    public ?string $lastname;
}

class Abteilung
{
    public string $name;

    // Die Eigenschaft "standort" muss ein Objekt der Klasse Standort sein:
    public Standort $standort;

    // Wenn hier (potentiell) mehrere Objekte der Klasse Staff enthalten sein sollen, gäbe es zwei Wege:
    // 1. (nicht typen-sicher) einfach Array (darin dann die Objekte)...
    // 2. (TODO: das Array in ein eigenes iterierbares Objekt einzukapseln)...
    /* @var array of Staff */
    public array $staff;
}

class Standort
{
    public string $adresse;
    public float $lat;
    public float $lon;
}

$person1 = new Staff;
$person1->firstname = 'John';
$person1->lastname = 'Doe';

$person2 = new Staff;
$person2->firstname = 'Jane';
$person2->lastname = 'Doe';

$wien = new Standort;
$wien->adresse = 'Musterstraße 1';
$wien->lat = 48.123231;
$wien->lon = 16.3124324;

$entwicklung = new Abteilung();
$entwicklung->name = "Entwicklungsabteilung";
$entwicklung->name = "Test";
$entwicklung->standort = $wien;
$entwicklung->staff = [ $person1, $person2, new Staff ];

// Zugriff über -> ... bei nested/verschachtelten Objekte von links nach rechts über Objekt->Eigenschaft/Objekt->Eigenschaft->usw.
// Solche Verschachtelungen hat man bei Arrays auch manchmal:
// Zugriff $arr['key'] - das liefert die value, wenn die value selbst ein Array ist - $arr['key']['nested-key'][usw.]
echo $entwicklung->standort->adresse;
echo "\n";
echo $entwicklung->staff[1]->firstname;
echo "\n";
echo $person2->firstname;
echo "\n";

var_dump($entwicklung);
