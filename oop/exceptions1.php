<?php
function test(int $a) {
    echo $a;
    $x = 5 + "5";
}

$i = 5.8;

// Der Klammeraffe vor einer Funktion unterbindet Fehlerausgaben in dieser Funktion.
// Keine gute Idee: falls Fehler, dann beheben.
// Ob Fehler ausgegeben werden sollen, sollte ein Server-Setting sein.
@test($i);

// Das wäre die Lösung
$i = (int) $i;
test($i);
