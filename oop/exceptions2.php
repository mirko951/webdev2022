<?php
// Beispiel aus: https://www.w3schools.com/php/php_mysql_connect.asp
$servername = "localhost2";
$username = "root";
$password = "";

try {
  $conn = new PDO("mysql:host=$servername;dbname=minis", $username, $password);
  // ...
  // set the PDO error mode to exception
  echo "Connected successfully";
}
catch(PDOException $e) {
    // Das ist eine schlechte Idee, da in diesem Fall die Fehlermeldung auch auf dem Live-Server angezeigt wird...
    // 1. Nichtssagend für User
    // 2. Technische Infos für mögliche Angriffe...
    // echo "Connection failed: " . $e->getMessage();

    // Wenn wir diese PDOException auffangen, dann nur für die Benutzer auf dem Live-Server, d.h. allgemein Info:
    echo 'Wir führen gerade Wartungsarbeiten durch.';

    // Falls wir keine Fehler mehr erwarten dürfen, dann kann die Fehlerbehandlung/Ausnahmebehandlung auch einfach entfallen!
    // Denn es ist ein Serversetting: auf Entwicklungsumgebungen werden alle Fehler angezeigt...
    // Auf Live-Umbegebungen keine Ausgaben...

    // vgl. https://phpdelusions.net/articles/error_reporting
}

echo "test";
