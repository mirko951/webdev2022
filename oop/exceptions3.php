<?php
// Falls in einem Projekt häufig der immer gleiche Exception-Code in einem catch Block vorkommt,
// könnte einfach ein Default-Handler "registriert" werden.
// Bspw. in einer config.php einbinden.

function myExceptionHandler($e)
{
    error_log($e);
    // Betrifft hier allgemein ALLE Exceptions, die nicht aufgefangen werden.
    // D.h. es sollte unterschieden werden: wenn in php.ini display_errors = On (Enwicklungsserver) -> Fehlermeldung ausgeben.
    // Wenn display_errors = Off (Liveserver) -> allgemeine Fehlerseite/Meldung ohne technische Details...
    if (filter_var(ini_get('display_errors'), FILTER_VALIDATE_BOOLEAN)) {
        echo $e;
    } else {
        echo "<h1>Wartungsarbeiten</h1>
              ...<br>
              Please try again later.";
    }
    exit;
}

set_exception_handler('myExceptionHandler');
/*
---- config.php end
*/

$servername = "localhost2";
$username = "root";
$password = "";

$conn = new PDO("mysql:host=$servername;dbname=minis", $username, $password);
