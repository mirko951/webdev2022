<?php
// config.php
function myExceptionHandler($e)
{
    error_log($e);
    // Betrifft hier allgemein ALLE Exceptions, die nicht aufgefangen werden.
    // D.h. es sollte unterschieden werden: wenn in php.ini display_errors = On (Enwicklungsserver) -> Fehlermeldung ausgeben.
    // Wenn display_errors = Off (Liveserver) -> allgemeine Fehlerseite/Meldung ohne technische Details...
    if (filter_var(ini_get('display_errors'), FILTER_VALIDATE_BOOLEAN)) {
        echo $e;
    } else {
        echo "<h1>Wartungsarbeiten</h1>
              ...<br>
              Please try again later.";
    }
    exit;
}

set_exception_handler('myExceptionHandler');
/*
---- config.php end
*/

$json = 'not found'; // angenommen: das käme von der API
$arr = json_decode($json, true);
if ($arr === null) {
    throw new Exception('JSON from API invalid...');
}
echo 'done';
