<?php
/**
 * https://www.php.net/manual/en/language.oop5.interfaces.php
 * Ähnlich wie abstrakte Klassen, aber Interfaces implementieren nichts.
 * 
 * 2 Zwecke:
 * a. um Klassen zu einem gemeinsamen "Datentyp" (genauer instanceof) zusammenzufassen, um
 *    1. bei Parameterübergaben dieses Interface als Datentyp angeben zu können
 *    2. um bpsw. damit zu sagen, dass eine Klasse (als Kind des Interfaces) gewisse Fähigkeiten hat
 * b. um wie bei abstrakten Klassen Methoden-Signaturen vorzugeben, um damit zu erzwingen, dass Klassen dieses Interfaces genau
 *    diese Methoden zu implementieren hat... um einheitliche Schnittstellen zu erzwingen.
 */

interface Database
{
    /**
     * CRUD
     */
    public function create(array $data);
    public function read(int $id);
    // public function update(array $data);
    // public function delete(int $id);
}

class MySql implements Database
{
    public function create(array $data)
    {
        // TODO:
        // INSERT INTO table (wert1, wert2) VALUES (?, ?)
        // exucte($data)...
    }
    public function read(int $id)
    {
        // TODO:
        // SELECT * FROM table WHERE id = ?
        // exucte([$id])...
        echo "debug lesen mysql";
    }
}

class FileDb implements Database
{
    public function create(array $data)
    {
        // TODO:
        // $fp = fopen('data.csv', 'a');
        // fputcsv(...);
    }
    public function read(int $id)
    {
        // TODO:
        // $fp = fopen('data.csv', 'a');
        // filepointer auf zeile $id
        // fgetcsv(...);
        echo "debug lesen CSV";
    }
}

$config = 'FileDb';

$test = new $config;
$test->read(1);
