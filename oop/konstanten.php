<?php
class Einhorn
{
    const HOERNER = 1;
    public string $name;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function printName()
    {
        echo '<strong>' . $this->name . '</strong>';
    }
}

echo Einhorn::HOERNER;

$peter = new Einhorn('Peter');
$test = new Einhorn('Test');
$test->name = 'Markus';
echo $test->name;

$test->printName();
