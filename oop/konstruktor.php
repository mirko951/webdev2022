<?php
/**
 * This class is our base class for staff
 */
class Staff
{
    /** @var string $firstname the firstname */
    public ?string $firstname;
    public ?string $lastname;
    private ?string $email;

    public function __construct(?string $firstname = null, ?string $lastname = null, ?string $email = null)
    {
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->setEmail($email);
    }

    public function __toString()
    {
        return $this->firstname . ' ' . $this->lastname;
    }

    /**
     * Validates and sets the email of the object
     * 
     * @param string $email the email of the staff-object
     * @return boolean whether the email is valid and set
     */
    public function setEmail(?string $email) : bool
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->email = $email;
            return true;
        }
        $this->email = null;
        return false;
    }
}

$person1 = new Staff('John', 'Doe', 'john@doe.com');
var_dump($person1);
$person2 = new Staff;
var_dump($person2);

echo $person1;
