<?php
namespace Math;
include 'functions.php';

function mb_strlen(string $var)
{
    // Bei Funktionen und Konstanten wird zuerst im definierten (siehe 1. Zeile) Namensraum nach der Funktion/Konstante gesucht,
    // wird keine gefunden, wird dann automatisch im globalen Namensraum ("scope") nach der Funktion gesucht.
    $test = strlen($var);

    // Bei Klassen ist es anders: hier wird nur im aktuellen Namensraum gesucht - wird keine gefunden -> Error.
    // D.h. bei Klassen müsste man immer mit \ ... auf den globalen Raum verweisen.
    // $obj = new \PDO('mysql:...');

    return $test;
}

/*
class PDO
{
    public int $x;
}
*/

echo mb_strlen("täst");

max();
