<?php
// https://www.php.net/manual/de/language.oop5.static.php
// https://de.wikipedia.org/wiki/Fabrikmethode
class Staff
{
    public string $firstname;
    public string $lastname;
    const TYP = "Mensch";

    public function __construct(string $fn = '', string $ln = '')
    {
        $this->firstname = $fn;
        $this->lastname = $ln;
    }

    public function save()
    {
        $db = new PDO('mysql:host=localhost;dbname=ma_verwaltung', 'root', '');
        $stmt = $db->prepare('INSERT INTO personal (vorname, nachname, svn, abt_id) VALUES (?, ?, 1234, 1)');
        $stmt->execute([$this->firstname, $this->lastname]);
    }

    public static function get($id)
    {
        $db = new PDO('mysql:host=localhost;dbname=ma_verwaltung', 'root', '');
        $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        $stmt = $db->prepare('SELECT * FROM personal WHERE id = ?');
        $stmt->execute([$id]);
        if ($row = $stmt->fetch()) {
            return new self($row['vorname'], $row['nachname']);
        } else {
            return false;
        }
    }
}

echo Staff::TYP;

$new = new Staff('Markus', 'Huber');
$new->save();

// TODO: get person with id 1
$old = Staff::get(1);
var_dump($old);
