<?php
class Staff
{
    public ?string $firstname;
    public ?string $lastname;
    protected ?string $email;

    public function __construct(?string $firstname = null, ?string $lastname = null, ?string $email = null)
    {
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->setEmail($email);
    }

    public function setEmail(?string $email) : bool
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->email = $email;
            return true;
        }
        $this->email = null;
        return false;
    }
}

class Freelancer extends Staff
{
    public ?float $fee;
    public ?bool $active;

    public function __construct(?string $firstname = null, ?string $lastname = null, ?string $email = null, ?float $fee = null, ?bool $active = null)
    {
        parent::__construct($firstname, $lastname, $email);
        $this->fee = $fee;
        $this->active = $active;

        // nur zum test (protected):
        $this->email = 'neu';
    }
}

class Employee extends Staff
{
    public ?float $salary;
    public ?float $hours;

    public function __construct(?string $firstname = null, ?string $lastname = null, ?string $email = null, ?float $salary = null, ?float $hours = null)
    {
        parent::__construct($firstname, $lastname, $email);
        $this->salary = $salary;
        $this->hours = $hours;
    }
}

$person1 = new Freelancer('Jane', 'Doe', 'jane@test.at', 120, true);
var_dump($person1);

$person2 = new Employee('Markus', 'Huber', 'test@test.at', 5000, 40);
var_dump($person2);

$person3 = new Staff;
var_dump($person3);

if ($person2 instanceof Employee) {
    echo "Ja, ist ein Employee / Staff";
}
