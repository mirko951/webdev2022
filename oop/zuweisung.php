<?php
// Datentypen (außer Objekte) werden per default als WERT-Zuweisung bzw. Wert-Übergabe behandelt:
$x = 5;
$y = $x;
$y = 6;

test($y);

function test($i)
{
    $i = 8;
    // echo $i;
}

// echo "test";

$arr = [ 1, 3, 4 ];
$x = $arr;


// https://www.php.net/manual/de/language.oop5.basic.php
// Bei Objekten verhält es sich anders:
// Eine Zuweisung (direkte Zuweisung oder Zuweisung über Funktions-Parameter, etc.) enthält immer das Objekt selbst:
// $obj2 = $obj1; bedeutet: $obj2 zeigt auf dasselbe Objekt wie $obj1... wenn $obj2 geändert wird, ändert sich auch $obj1
// Es ist also dasselbe(!) Objekt. (Ähnlich einer Referenzzuweisung.)
// Ein Objekt existiert also solange, solange es zumindest eine Variable gibt, die das Objekt "hält"...

class Klasse
{
    public int $x = 5;
}

$obj1 = new Klasse;
var_dump($obj1);

$obj2 = $obj1;
$obj2->x = 8;

var_dump($obj1);
